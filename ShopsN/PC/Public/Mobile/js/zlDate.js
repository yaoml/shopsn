﻿var obj = { date: new Date(), year: -1, month: -1, priceArr: [] };
var htmlObj = { header: "", left: "", right: "" };
var elemId = null;
function getAbsoluteLeft(objectId) {
   var o = document.getElementById(objectId)
   var oLeft = o.offsetLeft;
    while (o.offsetParent != null) {
        oParent = o.offsetParent
        oLeft += oParent.offsetLeft
        o = oParent
    }
    return oLeft
}
//获取控件上绝对位置
function getAbsoluteTop(objectId) {
   var o = document.getElementById(objectId);
   var oTop = o.offsetTop + o.offsetHeight + 10;
    while (o.offsetParent != null) {
        oParent = o.offsetParent
        oTop += oParent.offsetTop
        o = oParent
    }
    return oTop
}
//获取控件宽度
function getElementWidth(objectId) {
    x = document.getElementById(objectId);
    return x.clientHeight;
}
var pickerEvent = {
    Init: function (elemid) {
        if (obj.year == -1) {
            dateUtil.getCurrent();
        }
        for (var item in pickerHtml) {
            pickerHtml[item]();
        }
        var p = document.getElementById("calendar_choose");
        if (p != null) {
            document.body.removeChild(p);
        }
        var html = '<div id="calendar_choose" class="calendar" style="position:absolute;top:0; height:321px;">'
        html += htmlObj.header;
        html += '<div class="basefix" id="bigCalendar" style="display: block;">';
        html += htmlObj.left;
        html += htmlObj.right;
        html += '<div style="clear: both;"></div>';
        html += "</div></div>";
        elemId=elemid;
        var elemObj = document.getElementById(elemid);       
        $(document.body).append(html);
        document.getElementById("picker_last").onclick = pickerEvent.getLast;
        document.getElementById("picker_next").onclick = pickerEvent.getNext;
        document.getElementById("calendar_choose").style.zIndex = 1000;
    },
    getLast: function () {
        dateUtil.getLastDate();
        pickerEvent.Init(elemId);
    },
    getNext: function () {
        dateUtil.getNexDate();
        pickerEvent.Init(elemId);
    },
	getToday:function(){
		dateUtil.getCurrent();
		pickerEvent.Init(elemId);
	},
    setPriceArr: function (arr) {
        obj.priceArr = arr;
    },
    isShow: function () {
        var p = document.getElementById("calendar_choose");
        if (p != null) {
            return true;
        }
        else {
            return false;
        }
    }
}
var pickerHtml = {
    getHead: function () {
        var head = '<div class="calendar_left pkg_double_month"><p class="date_text"><a style="padding:30px;" href="javascript:void()" id="picker_last"> &lt; </a>&nbsp;' + obj.year + '年' + obj.month + '月&nbsp;<a style="padding:30px;" href="javascript:void()" id="picker_next"> &gt; </a></p></div><ul class="calendar_num" style="margin:0;"><li class="bold">日</li><li>一</li><li>二</li><li>三</li><li>四</li><li>五</li><li class="bold">六</li></ul>';
        htmlObj.header = head;
    },
    getRight: function () {
        var days = dateUtil.getLastDay();
        var week = dateUtil.getWeek();
        var html = '<table cellpadding="0" cellspacing="0" id="calendar_tab" class="calendar_right"><tbody>';
        var index = 0;
        for (var i = 1; i <= 42; i++) {
            if (index == 0) {
                html += "<tr>";
            }
            var c = week > 0 ? week : 0;
            if ((i - 1) >= week && (i - c) <= days) {
                var price = commonUtil.getPrice((i - c));
                var priceStr = "";
                var classStyle = "";
                if (price != -1) {
                    priceStr = "¥" + price;
                    classStyle = "class='on'";
                }
				if (price != -1&&obj.year==new Date().getFullYear()&&obj.month==new Date().getMonth()+1&&i-c==new Date().getDate()) {
                    classStyle = "class='on today'";
                }
				//判断今天
				if(obj.year==new Date().getFullYear()&&obj.month==new Date().getMonth()+1&&i-c==new Date().getDate()){
					if(priceStr == ''){
						html += '<td height="40"' + classStyle + ' id="' + obj.year + "-" + obj.month + "-" + (i - c) + '" price="' + price + '"><a class="date_a"><div style="color:#008AD4; font-size:14px;">今天</div><div class="calendar_price01">' + priceStr + '</div></a></td>';
					}else{
						html += '<td height="40" onClick="get_date(this)"' + classStyle + ' id="' + obj.year + "-" + obj.month + "-" + (i - c) + '" price="' + price + '"><a class="date_a"><div style="color:#008AD4; font-size:14px;">今天</div><div class="calendar_price01">' + priceStr + '</div></a></td>';
					}
				}
				else{
					if(priceStr == ''){
						html += '<td height="40" ' + classStyle + ' date="' + obj.year + "-" + obj.month + "-" + (i - c) + '" price="' + price + '"><a class="date_a"><div class="basefix" style="font-family:Georgia;">' + (i - c) + '</div><div class="calendar_price01">' + priceStr + '</div></a></td>';
					}else{
						html += '<td height="40" style="background:#FFF;" onClick="get_date(this)"' + classStyle + ' id="' + obj.year + "-" + obj.month + "-" + (i - c) + '" price="' + price + '"><a class="date_a"><div class="basefix" style="color:#000;font-family:Georgia;">' + (i - c) + '</div><div class="calendar_price01">' + priceStr + '</div></a></td>';	
					}                	
				}
                if (index == 6) {
                    html += '</tr>';
                    index = -1;
                }
            }
            else {
                html += "<td height='40'><a class='date_a'>&nbsp;</a></td>";
                if (index == 6) {
                    html += "</tr>";
                    index = -1;
                }
            }
            index++;
        }
        html += "</tbody></table>";
        htmlObj.right = html;
    }
}
var dateUtil = {
    //根据日期得到星期
    getWeek: function () {
        var d = new Date(obj.year, obj.month - 1, 1);
        return d.getDay();
    },
    //得到一个月的天数
    getLastDay: function () {
        var new_year = obj.year;//取当前的年份        
        var new_month = obj.month;//取下一个月的第一天，方便计算（最后一不固定）        
        var new_date = new Date(new_year, new_month, 1);                //取当年当月中的第一天        
        return (new Date(new_date.getTime() - 1000 * 60 * 60 * 24)).getDate();//获取当月最后一天日期        
    },
    getCurrent: function () {
        var dt = obj.date;
        obj.year = dt.getFullYear();
        obj.month = dt.getMonth() + 1;
		obj.day = dt.getDate();
    },
    getLastDate: function () {
        if (obj.year == -1) {
            var dt = new Date(obj.date);
            obj.year = dt.getFullYear();
            obj.month = dt.getMonth() + 1;
        }
        else {
            var newMonth = obj.month - 1;
            if (newMonth <= 0) {
                obj.year -= 1;
                obj.month = 12;
            }
            else {
                obj.month -= 1;
            }
        }
    },
    getNexDate: function () {
        if (obj.year == -1) {
            var dt = new Date(obj.date);
            obj.year = dt.getFullYear();
            obj.month = dt.getMonth() + 1;
        }
        else {
            var newMonth = obj.month + 1;
            if (newMonth > 12) {
                obj.year += 1;
                obj.month = 1;
            }
            else {
                obj.month += 1;
            }
        }
    }
}
var commonUtil = {
    getPrice: function (day) {
        var dt = obj.year + "-";
        if (obj.month < 10)
        {
            dt += "0"+obj.month;
        }
        else
        {
            dt+=obj.month;
        }
        if (day < 10) {
            dt += "-0" + day;
        }
        else {
            dt += "-" + day;
        }
        
        for (var i = 0; i < obj.priceArr.length; i++) {
            if (obj.priceArr[i].Date == dt) {
                return obj.priceArr[i].Price.split('.')[0];
            }
        }
        return -1;
    }
}