<?php
/**
 * ShopsN 著作权声明
 *===================
 *版权所有 2003-2017 上海亿速网络科技有限公司，并保留所有权利
 * 网站地址：http://www.shopsn.net
 *--------------------------------------------
 *这不是一个自由软件！免费使用时，您只能在保留完整版权声明的情况下对程序代码进行修改、使用。
 *在删除此版权声明之前如未获得商业授权，请务必与贵司负责人确认是否准备好接受侵权后果。
 *本系统不允许对程序代码以任何形式任何目的再发布各种修改版、插件等。
 *（不限于网站、手机网站、微商城、安卓原生商城、IOS原生商城、网页版商城APP）
 */

// 应用入口文件
header("Content-type:text/html;charset=utf-8");
// 检测PHP环境
if(version_compare(PHP_VERSION,'5.4.0','<'))  die('require PHP > 5.4.0 !');

if(file_exists("./install/") && !file_exists("./install/install.lock")){
    if($_SERVER['PHP_SELF'] != '/index.php'){
        exit("请在域名根目录下安装,如:<br/> www.xxx.com/index.php 正确 <br/>  www.xxx.com/www/index.php 错误,域名后面不能圈套目录, 但项目没有根目录存放限制,可以放在任意目录,apache虚拟主机配置一下即可");
    }
    header('Location:/install/index.php');
    exit();
}

// 开启调试模式 建议开发阶段开启 部署阶段注释或者设为false
define('APP_DEBUG',True);

//默认分页长度
define('PAGE_SIZE', 20);

// 定义应用目录
define('APP_PATH','./Application/');

if (strpos($_SERVER['PHP_SELF'], 'Admin') !== false || strpos($_SERVER['PHP_SELF'], 'admin') !== false)   
{
    echo file_get_contents('ErrorFiles/400.html');die();
}
require './Core/index.php';