<?php

/**
 * Created by PhpStorm.
 * User: jiangqingfeng
 * Date: 2016/7/10
 * Time: 18:38
 */
namespace Mobile\Model;

use Think\Model;
class MyzzyModel extends Model
{
    protected $trueTableName = 'vip_member';
    public function member_manage($id=null){
        $rows=$this->where(array('a.id'=>$id))->field('a.*,b.huifei_profit,b.consume_profit,b.tour_jifen_profit,b.Gross_income,b.tixian_money,b.surplus_money,b.join_profit,b.used_jifen')->join('as a LEFT JOIN vip_member_profit as b on a.id=b.member_id')->find();//用户自己的相关信息
        if($id!='10000000' && $rows!=null){

            $where['path']=array('like',$rows['path'].'%');
            $results=$this->where($where)->join('as a LEFT JOIN db_user as b on a.user_id=b.id')->field('a.*,b.username')->select();   //所有下级会员信息
            foreach($results as &$result){
                $result['path']=str_ireplace($rows['path'],'',$result['path']);     //将下家所有的path字段中本身的path进行替换
                if(substr_count( $result['path'],'-')!=0){
                    if(substr_count( $result['path'],'-')==1){
                        $first[]=$result;       //下一级所有用户
                    }elseif(substr_count( $result['path'],'-')==2){
                        $second[]=$result;      //下2级所有用户
                    }elseif(substr_count( $result['path'],'-')==3){
                        $third[]=$result;        //下三级所有用户
                    }else{
                        $others[]=$result;
                    }
                }
            }
            foreach($others as $other){
                $ids[]=$other['id'];
            }
            //下三级以后的所有会员ID
            //各级别会员人数
            return array(count($first),count($second),count($third));
            //上级会员信息
            //$upmember=$this->where(array('a.id'=>$rows['pid']))->join('as a LEFT JOIN db_user as b on a.user_id=b.id')->field('a.*,b.username')->find();
        }
    }
}