<?php
namespace Mobile\Controller;
use Think\Controller;
use Mobile\Model\GoodsClassModel;
use Common\Controller\CommonController;

/**
 * 前台控制器基类 
 */
class BaseController extends Controller
{
    /**
     * 读取导航(商品大分类) 
     */
    use CommonController;
    public function _initialize()
    {
        if (!S('nav_data')) {
            $model = M('goods_class');
            //查找导航 0为显示
            $data = $model->field('class_name,id')->where('fid =0 and hide_status = 0 and is_show_nav= 0')->select();
            
            S('nav_data', $data, 600);
        }
        
        $fid = isset($_GET['class_id']) ? $_GET['class_id'] : 0;
        $result_class = GoodsClassModel::getInition()->getClass(array(
           'where' => array('fid' => $fid, 'type' =>1, 'hide_status'=>0),
           'field' => array('class_name','id', 'pic_url'),
           'limit' => 10
        ));
       
        if (!empty($result_class))
        {
           foreach ($result_class as $key => &$value)
           {
               $value['class_name'] = strlen($value['class_name']) > 5 ? \Common\Tool\Tool::cut_str($value['class_name'], 4) : $value['class_name'];
           }
        }
        $this->result_class  = $result_class;
        $this->nav_data      = S('nav_data');
        $this->intnetTitle   = $this->getConfig('intnet_title');
    }
    
    /**
     * ajax 返回数据
     */
    protected function ajaxReturnData($data, $status= 1, $message = '操作成功')
    {
        $this->ajaxReturn(array(
            'status'  => $status,
            'message' => $message,
            'data'    => $data
        ));
        die();
    }
    /**
     * 提示client
     * @param array   $data     要检测的数据
     * @param string  $checkKey 要检测的键
     * @param string  $message  信息
     */
    protected function prompt( $data, $checkKey, $message , $isValidate = true)
    {
        if (empty($data)) {
            $this->ajaxReturnData(null, '400', $message);
        } elseif(is_array($data) && empty($data[$checkKey]) && $isValidate ) {
            $this->ajaxReturnData(null, '400', $message);
        }
        return true;
    }
    
    protected function updateClient($insert_id, $messagePrfix = '更新', $isNull = FALSE)
    {
        $status    = empty($insert_id) ? '400' : '200';
        $message   = empty($insert_id) ? '空数据' : $messagePrfix.'成功';
        $insert_id = $isNull === true ? null : $insert_id;
        $this->ajaxReturnData($insert_id, $status, $message);
    }
}