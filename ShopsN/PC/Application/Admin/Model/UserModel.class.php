<?php
namespace Admin\Model;

use Think\Model;

/**
 * 用户模型
 */
class UserModel extends Model
{
    
    private static $obj ;
    
    public static function getInitation()
    {
        $class = __CLASS__;
        return self::$obj = !(self::$obj instanceof $class) ? new self() : self::$obj;
    }
    
    /**
     * 根据订单信息 查询用户信息
     */
    public function userInfoByOrder(array $orderData, $field)
    {
        if (! is_array($orderData) || empty($orderData['user_id']) || empty($field)) {
            return array();
        }
        return $userInfo = $this ->field($field) ->where('id = "%s"', $orderData['user_id'])->find();
    }
}