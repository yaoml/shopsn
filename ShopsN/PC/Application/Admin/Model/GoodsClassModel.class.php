<?php
namespace Admin\Model;

use Think\Model;

/**
 * 商品分类模型
 */
class GoodsClassModel extends Model
{
    /**
     * 更新数据
     */
    private static $obj ;
    
    
    public function save(array $data, $options = '')
    {
        if (empty($data))
        {
            return 0;
        }
        $data = $this->create($data);
        
        return parent::save($data, $options);
    }
    
    /**
     * 获取该类的实例 
     */
    public static  function getInition()
    {
        return  self::$obj= !(self::$obj instanceof GoodsClassModel) ? new self() : self::$obj;
    }
    
    /**
     * 添加前操作
     */
    protected function _before_insert(&$data,$options)
    {
        $data['create_time'] = time();
        $data['type']        = 1;
        $data['update_time'] = time();
        return $data;
    }
    
    /**
     * 重写添加操作 
     */
    public function add($data, array $options = array(), $replace = false)
    {
        if (empty($data))
        {
            return 0;
        }
        
        $data = $this->create($data);
        return parent::add($data, $options, $replace);
    }
    /**
     * 重写查询操作 
     */
    public function select(array $options = array())
    {
        if (empty($options))
        {
            return array();
        }
        
        $data = parent::select($options);
        
        foreach ($data as $key => &$value)
        {
            $value['create_time'] = date('Y-m-d H:i:s',$value['create_time']);
            $value['vo'] = parent::select(array(
                'where' => array('fid' => $value['id']),
                'field' => array('id', 'class_name', 'pic_url', 'sort_num'),
                'order' => array('create_time DESC')
            ));
        }
        return $data;
    }
    
    //获取全部编号
    public function getAllClassId(array $options)
    {
        if (empty($options))
        {
            return array();
        }
        
        return parent::select($options);
    }
    //更新前操作
    protected function _before_update(&$data, $options)
    {
        $data['update_time'] = time();
        return $data;
    }
    
    /**
     * 获取全部子集分类
     * @param array $where 查询条件
     * @param array $field 查询的字段
     * @return string
     */
    public function getChildren(array $where = null, array $field = null)
    {
        // 根据地区编号  查询  该地区的所有信息
        $video_data   = parent::select(array(
            'where' => $where,
            'field' => $field,
        ));
        if (empty($video_data))
        {
            return array();
        }
        $pk    = $this->getPk();
        static $children = array();
        foreach ($video_data as $key => &$value)
        {
            if(!empty($value[$pk]))
            {
                $where['fid'] = $value[$pk];
                $child = $this->getChildren(array('fid' => $value[$pk]), $field);
                $children[$key] = $value;
                if (!empty($child))
                {
                    $children[$key]['children'] = $child;
                }
                unset($video_data[$key], $child);
            }
        }
        return $children;
    }
    
}