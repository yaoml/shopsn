<?php
namespace Admin\Model;

use Think\Model;

class AuthGroupModel extends Model
{
    private static  $obj;
    public static function getInitnation()
    {
        $name = __CLASS__;
        return self::$obj = !(self::$obj instanceof $name) ? new self() : self::$obj;
    }
    
    public function getAuthGroupById($field, $where = null, $fun = 'select')
    {
        if (empty($field))
        {
            return array();
        }
        return $this->field($field)->where($where)->$fun();
    }
    
    /**
     * {@inheritDoc}
     * @see \Think\Model::save()
     */
    public function save($data='', $options=array())
    {
        if (empty($data))
        {
            return false;
        }
        $data = $this->create($data);
        return parent::save($data, $options);
    }
}