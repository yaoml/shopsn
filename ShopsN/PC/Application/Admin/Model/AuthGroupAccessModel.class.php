<?php
namespace Admin\Model;

use Think\Model;

/**
 * 管理员-分组模型 
 */
class AuthGroupAccessModel extends Model
{
    private static  $obj;
    public static function getInitnation()
    {
        $name = __CLASS__;
        return self::$obj = !(self::$obj instanceof $name) ? new self() : self::$obj;
    }
    
    /**
     * 重写添加操作
     */
    public function add($data = '', $options = array(),  $replace = false)
    {
        if (empty($data) || !is_array($data))
        {
            return array();
        }
         
        $addData  = $this->create($data);
        return parent::add($addData, $options, $replace);
    }
    

    public function getAuthGroupById($field, $where = null, $fun = 'select')
    {
        if (empty($field))
        {
            return array();
        }
        return $this->field($field)->where($where)->$fun();
    }
    
    /**
     * {@inheritDoc}
     * @see \Think\Model::save()
     */
    public function save($data='', $options=array())
    {
        if (empty($data))
        {
            return false;
        }
        $data['uid'] = $data['id'];
        $data = $this->create($data);
        return parent::save($data, $options);
    }
}