<?php
/*
 * @thinkphp3.2.2  auth认证   php5.3以上
 * @Created on 2015/08/18
 * @Author  夏日不热(老屁)   757891022@qq.com
 *
 */
namespace Admin\Controller;
use Common\Controller\AuthController;
use Think\Auth;

//管理
class CommentController extends AuthController {
	
	//列表
    public function comment_list(){
    	$m = M('comment');
    	$nowPage = isset($_GET['p'])?$_GET['p']:1;
    	if(!empty($_POST['title'])){
    		$where['title'] = array('like','%'.$_POST['title'].'%');
    	}
     	if(!empty($_POST['keyword'])){
    		$where['keyword'] = array('like','%'.$_POST['keyword'].'%');
    	}
    	// page方法的参数的前面部分是当前的页数使用 $_GET[p]获取
    	$result = $m->where($where)->order('id DESC')->page($nowPage.','.PAGE_SIZE)->select();
    	$nid = count($result);
    	foreach ($result as $k=>$v){
    		$result[$k]['create_time'] = date('Y-m-d',$v['create_time']);
    		$result[$k]['nid'] = $nid--;
    	}
    	//分页
    	$count = $m->where($where)->count(id);		// 查询满足要求的总记录数
    	$page = new \Think\Page($count,PAGE_SIZE);		// 实例化分页类 传入总记录数和每页显示的记录数
    	$show = $page->show();		// 分页显示输出
    	$this->assign('page',$show);// 赋值分页输出
    	$this->assign('result',$result);
    	$this->display();
    }
    
    //删除评论
    public function comment_del(){
    	$where['id'] = $_POST['id'];	//评论ID
    	$m = M('comment');
    	$result = $m->where($where)->delete();
    	if($result){
    		$data['code'] = '1';	//删除成功
    		$this->ajaxReturn($data);
    	}else{
    		$data['code'] = '0';	//删除失败
    		$this->ajaxReturn($data);
    	}
    }

}




