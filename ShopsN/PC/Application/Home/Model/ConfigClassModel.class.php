<?php
namespace Home\Model;

use Think\Model;

class ConfigModel extends Model
{
    private static $obj;
    
    public static function getInitnation()
    {
        $name = __CLASS__;
        return self::$obj = !(self::$obj instanceof $name) ? new self() : self::$obj;
    }
    
    public function getAllConfig(array $option = null)
    {
        return $this->where($option)->select();
    }
}