<?php
namespace Home\Model;
use Think\Model;

/**
 * 广告分类表 
 */
class AdSpaceModel extends Model
{
    /**
     * 查询分类 及其子分类 
     */
    public function select($options = array(), Model $model)
    {
        if (!($model instanceof Model) || !is_object($model))
        {
            return array();
        }
        $data = parent::select($options);
        if (!empty($data))
        {
            foreach ($data as $key => &$value)
            {
                $value['children'] = $model->select(array(
                    'where' => array('ad_space_id' => $value['id'], 'type' => array('in','1,3,4') ,'isshow' => 0),
                    'order' => array('sort_num' => 'DESC', 'update_time' => 'DESC', 'create_time' => 'DESC'),
                    'field' => array('ad_link', 'id', 'title', 'pic_url'),
                    'limit' => 4
                ));
                
                if (empty($value['children']))
                {
                    unset($data[$key]);
                }
            }
        }
        return $data;
    }
    
} 