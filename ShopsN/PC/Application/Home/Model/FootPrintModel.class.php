<?php
namespace Home\Model;

use Think\Model\AdvModel;

/**
 * 我的足迹模型
 * @author 王强 
 */
class FootPrintModel extends AdvModel
{
    private static $obj ;
    
    const selectNumber = 5;
    
    protected $noSelect = 'create_time,uid';
        
    public static function getInitation()
    {
        $class = __CLASS__;
        return self::$obj = !(self::$obj instanceof $class) ? new self() : self::$obj;
    }
    
    public function add($data, $options = array(), $replace = false)
    {
        if (empty($data))
        {
            return false;
        }
        if ($this->where('gid = "'.$data['gid'].'"')->getField($this->getPk()))
        {
            return false;
        }
        $data = $this->create($data);
        $insertId = parent::add($data);
        
        return  $insertId;
    }
    
    protected function _before_insert(& $data, $options)
    {
        $data['create_time'] = time();
        return $data;
    }
    
    /**
     * 我的足迹 
     */
    public function getFootprintUserId($options, $default='select')
    {
        if (empty($options))
        {
            return array();
        }
        return $this->$default($options);
    }
    /**
     * @return the $noSelect
     */
    public function getNoSelect()
    {
        return $this->noSelect;
    }

    /**
     * @param string $noSelect
     */
    public function setNoSelect($noSelect)
    {
        $this->noSelect = $noSelect;
    }

}
