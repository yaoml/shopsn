<?php 
namespace API\Model;

use Think\Model;

/**
 * 收货地址模型 
 */
class AddressModel extends Model
{
    private static $obj ;
    
    public static function getInitation()
    {
        $class = __CLASS__;
        return self::$obj = !(self::$obj instanceof $class) ? new self() : self::$obj;
    }
    
    protected function _before_update( &$data, $options)
    {
        $data['update_time'] = time();
        return $data;
    }
     
    protected function _before_insert(&$data, $options)
    {
        $data['update_time'] = time();
        $data['create_time'] = time();
        $data['user_id']     = $_SESSION['userId'];
        return $data;
    }
    
    public function add($data='', $options=array(), $replace=false)
    {
        if (empty($data))
        {
            return false;
        }
        $data = $this->create($data);
        return parent::add($data, $options, $replace);
    }
    
    public function save($data = '', $options = array())
    {
        if (empty($data))
        {
            return false;
        }
        $data = $this->create($data);
        return parent::save($data, $options);
    }
    
   
}