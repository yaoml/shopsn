<?php
namespace API\Model;

use Think\Model;

/**
 * 用户收货地址模型 
 */
class UserAddressModel extends Model
{
    private static $obj ;
    
    public static function getInitation()
    {
        $class = __CLASS__;
        return self::$obj = !(self::$obj instanceof $class) ? new self() : self::$obj;
    }
    
    protected function _before_update( &$data, $options)
    {
        $data['update_time'] = time();
        return $data;
    }
     
    protected function _before_insert(&$data, $options)
    {
        $data['update_time'] = time();
        $data['create_time'] = time();
        $data['user_id']     = $_SESSION['userId'];
        return $data;
    }
    
    public function add($data='', $options=array(), $replace=false)
    {
        if (empty($data))
        {
            return false;
        }
        $data = $this->create($data);
        return parent::add($data, $options, $replace);
    }
    
    public function save($data = '', $options = array())
    {
        if (empty($data))
        {
            return false;
        }
        $data = $this->create($data);
        return parent::save($data, $options);
    }
    
    /**
     * 查询地址列表
     */
    public function getAddressList()
    {
        return $this
        ->field('create_time,update_time,user_id', true)
        ->where('user_id ="%s"', $_SESSION['userId'])
        ->order('create_time DESC,update_time DESC')
        ->select();
    }
    /**
     * 查新默认地址 
     */
    public function getDefaultByUser()
    {
        return $this
        ->field('create_time,update_time,user_id', true)
        ->where('user_id ="%s" and status = 1', $_SESSION['userId'])
        ->order('create_time DESC,update_time DESC')
        ->find();
    }
}