<?php
namespace API\Model;

use Think\Model;

class NewsModel extends Model
{
    private static $obj ;
    
    public static function getInitation()
    {
        $class = __CLASS__;
        return self::$obj = !(self::$obj instanceof $class) ? new self() : self::$obj;
    }
    
    /**
     * 获取系统消息 
     */
    public function getSystemMessage($where)
    {
        if (empty($where))
        {
            return array();
        }
        return $this->where($where)->order('create_time DESC')->select();
    }
}