<?php
namespace  API\Model;
use Think\Model;
use Common\Tool\Tool;

/**
 * 购物车 模型 
 */
class GoodsCartModel extends Model
{
    use \Common\TraitClass\callBackClass;
    private static $obj;
    // 添加购物车
    public function addCart(array $data)
    {
        if (empty($data) || !is_array($data))
        {
            return array();
        }
        $result = $this->field('id,goods_num')->where('user_id = "'.$_SESSION['userId'].'" and goods_id = "'.$data['goods_id'].'"')->find();
        //购物车中无商品，添加一条新信息，购物车中已有信息，则数量 +1
        $data = $this->create($data);
        $data['user_id'] = $_SESSION['userId'];
        $id = 0;
        if(empty($result)){
            $id   = $this->add($data);
        }else{
            $data['goods_num'] = $result['goods_num'] +  $data['goods_num'];
            $id = parent::save($data, array(
                'where' => array('id' => $result['id'])
            ));
        }
        return empty($id) ? false : true;
    }
    
    protected function _before_update( &$data, $options)
    {
        $data['update_time'] = time();
        return $data;
    }
    
    protected function _before_insert(&$data, $options)
    {
        $data['update_time'] = time();
        $data['create_time'] = time();
        return $data;
    }
    
    /**
     * 获取购物车数量 
     */
    public function getCartCount(array $options)
    {
       $isSuccess =  \Common\Tool\Tool::checkPost($options);
       
       if (!$isSuccess) {
           return false;
       }
       
       $count = $this->where($options)->count();
       
       return $count;
    }
   
    /**
     * 获取最新添加购物车的商品 
     */
    public function getNewCartForGood(array $options, Model $model)
    {
        $isSuccess =  \Common\Tool\Tool::checkPost($options);
         
        if (!$isSuccess || !($model instanceof Model)) {
            return false;
        }
        
        $data =  $this->find($options);
       
        if (!empty($data)) {
           $goods = $model->field('title,pic_url,description')->where('id = "'.$data['goods_id'].'"')->find();
           $data = array_merge((array)$goods, $data);
        }
        return $data;
    }
    
    public function select(array $options, Model $model)
    {
        if (empty($options) || !($model instanceof Model))
        {
            return $options;
        }
       
        $data = parent::select($options);
        
        if (empty($data))
        {
            return $data;
        }
        usort($data, array($this, 'compare'));
        
        $id = Tool::characterJoin($data);
        
        $goods = $model->field('id as goods_id,taocan as taocan_name,title,pic_url,price_new,fanli_jifen,is_baoyou')->where('id in ('.$id.')')->select();
        
        if (empty($goods))
        {
            return $goods;
        }
        
        //排序
        usort($goods, array($this, 'compare'));
        $data  = Tool::parseTwoArray($data, $goods, 'goods_id',array('title', 'pic_url', 'taocan_name','price_new','fanli_jifen','is_baoyou'));
        return $data;
    }
    
    public function save($data, $options)
    {
        if (empty($data) || empty($options))
        {
            return false;
        }
        
        $data = $this->create($data);
        
        return parent::save($data, $options);
    }
    
    public function getGoodCartById(array $options)
    {
        if (empty($options))
        {
            return $options;
        }
        $goodsCartData = parent::select($options);
        if (empty($goodsCartData) || count($goodsCartData) === 1)
        {
            return $goodsCartData;
        }
        usort($goodsCartData, array($this, 'compare'));
        return $goodsCartData;
    }
    
    
    public static function getInition()
    {
        $class = __CLASS__;
        return !(self::$obj instanceof $class) ? self::$obj = new self() : self::$obj;
    }
} 