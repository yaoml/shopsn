<?php
namespace API\Controller;
use Common\Tool\Tool;
use API\Model\OrderGoodsModel;
use API\Model\OrderCommentModel;
use API\Model\UserModel;
use API\Model\GoodsModel;

/**
 * 评论
 * @author Administrator
 */
class CommentController extends BaseController
{
    /**
     * 评论
     */
    public function index()
    {
        $this->isLogin();
        
        Tool::checkPost($_POST, array('is_numeric' => array('goods_orders_id', 'status')), true , array('goods_orders_id', 'content', 'status')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
       
        $id = OrderCommentModel::getInitation()->isCommentGoodsByUser($_POST['goods_orders_id']);
        $this->alreadyInData($id, '您已经评论过');
        //根据订单查询 订单下的商品
        $goods = OrderGoodsModel::getInitnation()->getGoodsIdByOrderId($_POST['goods_orders_id']);
        
        //为每个上商品追加数据
        Tool::connect('ArrayChildren');
        
        $goods = Tool::addValueAndKey($goods, $_POST);
        $status = OrderCommentModel::getInitation()->addAll($goods);
        
        $this->updateClient($status,'添加', true);
    }
    
    //商品评论
    public function goodsComment()
    {
        Tool::checkPost($_POST, array('is_numeric' => array('goods_id')), true , array('goods_id')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
    
        //连接工具
        Tool::connect('ArrayParse');
    
        $data = OrderCommentModel::getInitation()->select(array(
            'field' => 'id,content,status,create_time,user_id,goods_id',
            'where' => array('goods_id' => $_POST['goods_id'])
        ), UserModel::getInistnation());
        
        Tool::connect('parseString', null);
        //传递给商品模型 查询商品名
        $data = GoodsModel::getInitation()->getGoodsNameById($data);
        $this->ajaxReturnData($data);
    }
}