<?php
namespace API\Controller;

use API\Model\OrderModel;
use Common\Tool\Tool;
use Think\Controller;
use API\Model\OrderWxpayModel;

class NofityController extends Controller
{
    const PARTNER_ID = "abcdefghijklmnopqrstuvwxyz123456";
    public function wxNotify()
    {
        // 获取通知的数据
        $xml = $GLOBALS['HTTP_RAW_POST_DATA'];
       
    
        Tool::connect('Token');
    
        $data = Tool::init($xml, self::PARTNER_ID);
        
        
        if(empty($data['out_trade_no'])) {
            echo 'ERROR';
        }
       
        $orderId = substr($data['out_trade_no'],strpos($data['out_trade_no'], '-')+1);
        if (!is_numeric($orderId)) {
            echo 'ERROR';
        }
        //获取订单状态
        $ordersStatus = OrderModel::getInitation()->getOrderStatusByUser($orderId, 'id');
        
        $status = false;
        if ($ordersStatus == 0)
        {
            //修改状态
            $status =  OrderModel::getInitation()->save(array(
                'order_status' => OrderModel::YesPaid
            ), array(
                'where' => array('id' => $orderId)
            ));
            
            $status = OrderWxpayModel::getInitation()->save(array('status' => 1), array('where' => array('order_id' => $orderId)));
             
        }
        echo $status ? "SUCCESS" : 'ERROR';
    }
    /**
     * ajax 返回数据
     */
    protected function ajaxReturnData($data, $status= '200', $message = '操作成功')
    {
        $this->ajaxReturn(array(
            'code'  => $status,
            'message' => $message,
            'data'    => empty($data) ? null : $data
        ));
        die();
    }
}