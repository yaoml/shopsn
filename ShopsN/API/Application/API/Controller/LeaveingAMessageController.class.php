<?php
namespace API\Controller;

use Common\Tool\Tool;
use API\Model\AdviseModel;
use API\Model\UserModel;
use API\Model\NewsModel;

class LeaveingAMessageController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        
        Tool::checkPost($_POST, (array)null, false, array('token')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
        
        $this->isLogin();
    }
    
    public function liuyan()
    {
        Tool::checkPost($_POST, array('is_numeric' => array('mobile')), false, array('content', 'mobile', 'app_type')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
       
        //验证手机
        Tool::connect('ParttenTool');
        
        $status = Tool::validateData($_POST['mobile'], 'mobile');
        
        $this->prompt($status, null, '手机格式有误');
        
        $status = AdviseModel::getInitation()->add($_POST);
       
        $this->updateClient($status, '留言', true);
     
    }
        // 消息
    public function  news()
    {
        $data = NewsModel::getInitation()->getSystemMessage('sendto in (0,"'.$_SESSION['userId'].'")');
        $this->updateClient($data, '操作');
    }
}