<?php
namespace Common\Tool\Extend;

use Common\Tool\Tool;

class File extends Tool 
{
    
    public function parseFile(array $files, $setKey ='tmp_name')
    {
        if (empty($files))
        {
            return false;
        }
        /*一个图片时*/
        foreach ($files as $key => &$value)
        {  
            if (empty($value[$setKey])) 
            {
                continue;
            }
            $value[$setKey] = stripcslashes($value[$setKey]);
        }
        return $files;
    }
}