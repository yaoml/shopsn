//
//  ZMessageNewCell.h
//  shopSN
//
//  Created by yisu on 16/10/11.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface ZMessageNewCell : BaseTableViewCell

/** 消息图片 */
@property (nonatomic, strong) UIImageView *headIV;

/** 标示图片 */
@property (nonatomic, strong) UIImageView *flagIV;

/** 标题label */
@property (nonatomic, strong) UILabel *titleLb;

/** 详情label */
@property (nonatomic, strong) UILabel *detailLb;

/** 时间label */
@property (nonatomic, strong) UILabel *timeLb;




@end
