//
//  ZCenterShareQRCoderCell.h
//  shopSN
//
//  Created by yisu on 16/9/18.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 shopSN分享 页面
 *
 *   推广二维码 cell
 *
 */

#import "BaseTableViewCell.h"

@interface ZCenterShareQRCoderCell : BaseTableViewCell

/** 图片 */
@property (nonatomic, strong) UIImageView *cs_iconIV;

/** 标题 */
@property (nonatomic, strong) UILabel *cs_titleLb;

/** 二维码图片 */
@property (nonatomic, strong) UIImageView *cs_ewIV;


@end
