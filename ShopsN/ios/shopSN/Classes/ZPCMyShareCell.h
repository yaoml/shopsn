//
//  ZPCMyShareCell.h
//  shopSN
//
//  Created by chang on 16/7/7.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 我的模块 主页面
 *
 *  我的分享 cell
 *
 */
#import "BaseTableViewCell.h"
//subView
#import "ZPCMyOrderSubView.h"
#import "ZPCMyShareSubView.h"

@class ZPCMyShareCell;
@protocol ZPCMyShareCellDelegate <NSObject>

- (void)didShareButton:(UIButton *)sender;

@end
@interface ZPCMyShareCell : BaseTableViewCell

/** 用户 积分 */
@property (nonatomic, copy) NSString *points;

/** 用户 优惠券 */
@property (nonatomic, copy) NSString *discount;

/** 用户 余额 */
@property (nonatomic, copy) NSString *balance;

@property (nonatomic, strong) ZPCMyOrderSubView *orderSubView;
@property (nonatomic, strong) ZPCMyShareSubView *shareSubView;
/** 分享cell 代理*/
@property (weak, nonatomic) id<ZPCMyShareCellDelegate>delegate;

-(void)updateCellWith:(NSString *)point;



@end
