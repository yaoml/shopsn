//
//  ZCenterPrivilegeCell.m
//  shopSN
//
//  Created by yisu on 16/7/15.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCenterPrivilegeCell.h"

@implementation ZCenterPrivilegeCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        bgView.backgroundColor = HEXCOLOR(0xffffff);
        [self.contentView addSubview:bgView];
        
        [self initSubView:bgView];
    }
    return self;
}

- (void)initSubView:(UIView *)bgView {
    
    //1 图片
    _privilegeIV = [[UIImageView alloc] initWithFrame:CGRectMake((CGRectW(bgView)-40)/2, 10, 40, 40)];
    [bgView addSubview:_privilegeIV];
    _privilegeIV.backgroundColor = __TestOColor;
    _privilegeIV.layer.cornerRadius = 20;
    
    
    //2 标题 color(173 118 63)  (0xad763f)
    _privilegeTitleLb = [[UILabel alloc] initWithFrame:CGRectMake((CGRectW(bgView)-100)/2, CGRectYH(_privilegeIV)+5, 100, 20)];
    [bgView addSubview:_privilegeTitleLb];
//    _privilegeTitleLb.backgroundColor = __TestGColor;
    _privilegeTitleLb.font = MFont(10);
    _privilegeTitleLb.textColor = HEXCOLOR(0xad763f);
    _privilegeTitleLb.textAlignment = NSTextAlignmentCenter;
    _privilegeTitleLb.text = @"极速退款";
    
    //3 描述 极速达人 每月15日礼不见不散
    _privilegeDetailLb = [[UILabel alloc] initWithFrame:CGRectMake((CGRectW(bgView)-100)/2, CGRectYH(_privilegeTitleLb), 100, 20)];
    [bgView addSubview:_privilegeDetailLb];
    _privilegeDetailLb.font = MFont(10);
    _privilegeDetailLb.textColor = HEXCOLOR(0x999999);
    _privilegeDetailLb.textAlignment = NSTextAlignmentCenter;
    _privilegeDetailLb.text = @"每月15日礼不见不散";
    
    
    
}


@end
