//
//  ZHomeGridGoodsCell.h
//  shopSN
//
//  Created by yisu on 16/8/11.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 首页页面 collectionView
 *
 *  会员商品展示 cell
 *
 */
#import <UIKit/UIKit.h>

@interface ZHomeGridGoodsCell : UICollectionViewCell


/** 商品展示 商品图片 */
@property (nonatomic, strong) UIImageView *goodsIV;

/** 商品展示 商品标题 */
@property (nonatomic, strong) UILabel *goodsTypeLb;

/** 商品展示 商品标题 */
//@property (nonatomic, strong) UILabel *goodsTitleLb;

/** 商品展示 商品描述 */
@property (nonatomic, strong) UILabel *goodsDetailLb;

/** 商品展示 商品价格 */
@property (nonatomic, strong) UILabel *goodsPriceLb;

/** 商品展示 商品积分 */
@property (nonatomic, strong) UILabel *goodsPointsLb;

@end
