//
//  ZCommonGoodsDetailView.h
//  shopSN
//
//  Created by yisu on 16/8/2.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 普通商品 信息页面
 *
 *  详情 视图
 *
 */
#import <UIKit/UIKit.h>




@interface ZCommonGoodsDetailView : UIView




/** 普通商品简介页面 加载商品详情图片数组 */
//- (void)loadCommonGoodsImage:(NSArray *)array;

/** 普通商品简介页面 加载商品详情商品id */
- (void)loadCommonGoodsID:(NSString *)goodsID;

@end
