//
//  ZCMyOrderWaitingReceiveFooter.m
//  shopSN
//
//  Created by yisu on 16/7/14.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCMyOrderWaitingReceiveFooter.h"

@interface ZCMyOrderWaitingReceiveFooter ()
{
    UIButton *receivedSureBtn;//确认收货
    UIButton *deliveryInfoBtn;//查看物流
    
    //类商品 信息内容
    UIView *_tourismInfoView;
    UILabel *adultsLb;
    UILabel *minorsLb;
}
@end

@implementation ZCMyOrderWaitingReceiveFooter

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        
        //1 背景
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [self addSubview:bgView];
        
        //2 价格信息 h105
        UIView *priceInfoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(bgView), 105)];
        //priceInfoView.backgroundColor = __TestGColor;
        [bgView addSubview:priceInfoView];
        [self addPriceInfoSubViews:priceInfoView];
        
        
        //确认收货按钮
        receivedSureBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectW(self)-10-75, CGRectYH(priceInfoView)+10, 75, 25)];
        [self addSubview:receivedSureBtn];
        receivedSureBtn.titleLabel.font = MFont(11);
        receivedSureBtn.backgroundColor = __DefaultColor;
        receivedSureBtn.layer.cornerRadius = 3.0f;
        [receivedSureBtn setTitle:@"确认收货" forState:BtnNormal];
        [receivedSureBtn setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
        receivedSureBtn.tag = 1310;
        [receivedSureBtn addTarget:self action:@selector(waitReceiveButtonAction:) forControlEvents:BtnTouchUpInside];
        
        //查看物流按钮 边框color(169 169 169) (0xa9a9a9)
        deliveryInfoBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectW(self)-10*2-75*2, CGRectYH(priceInfoView)+10, 75, 25)];
        [self addSubview:deliveryInfoBtn];
        deliveryInfoBtn.titleLabel.font = MFont(11);
        //deliveryInfoBtn.backgroundColor = __TestOColor;
        deliveryInfoBtn.layer.borderWidth = 1.0f;
        deliveryInfoBtn.layer.borderColor = HEXCOLOR(0xa9a9a9).CGColor;
        deliveryInfoBtn.layer.cornerRadius = 3.0f;
        [deliveryInfoBtn setTitle:@"查看物流" forState:BtnNormal];
        [deliveryInfoBtn setTitleColor:HEXCOLOR(0x333333) forState:BtnNormal];
        deliveryInfoBtn.tag = 1311;
        [deliveryInfoBtn addTarget:self action:@selector(waitReceiveButtonAction:) forControlEvents:BtnTouchUpInside];
         deliveryInfoBtn.hidden = YES;
        
    }
    return self;
}


#pragma mark - 数量和金额
- (void)addPriceInfoSubViews:(UIView *)view {
    
    //0 信息
    _tourismInfoView = [[UIView alloc] initWithFrame:CGRectMake(10, 5, CGRectW(view)-20, 40)];
    [view addSubview:_tourismInfoView];
    //_tourismInfoView.backgroundColor = __TestOColor;
    _tourismInfoView.hidden = YES;
    
    adultsLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectW(_tourismInfoView), 20)];
    [_tourismInfoView addSubview:adultsLb];
    adultsLb.font = MFont(12);
    adultsLb.textColor = __DefaultColor;
    adultsLb.text = @"成人：人数_2 单价_￥2999.00  小计:￥5998.00";
    
    
    minorsLb = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectYH(adultsLb), CGRectW(_tourismInfoView), 20)];
    [_tourismInfoView addSubview:minorsLb];
    minorsLb.font = MFont(12);
    minorsLb.textColor = __DefaultColor;
    minorsLb.text = @"儿童：人数_2 单价_￥1999.00  小计:￥3998.00";
    
    //1 小计 价格 color (254 65 0) (fe4100)
    _settlePriceLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(view)-130, CGRectH(view)-50, 120, 15)];
    [view addSubview:_settlePriceLb];
    //_settlePriceLb.backgroundColor = __TestGColor;
    _settlePriceLb.font = MFont(11);
    _settlePriceLb.textColor = __MoneyColor;
    _settlePriceLb.textAlignment = NSTextAlignmentRight;
    _settlePriceLb.text = @"应付金额:￥589.00";
    
    
    //2 小计 商品件数
    _settleCountLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectX(_settlePriceLb)-100, CGRectH(view)-50, 100, 15)];
    [view addSubview:_settleCountLb];
    //_settleCountLb.backgroundColor = __TestOColor;
    _settleCountLb.font = MFont(11);
    _settleCountLb.textColor = HEXCOLOR(0x333333);
    _settleCountLb.textAlignment = NSTextAlignmentRight;
    _settleCountLb.text = @"小计(共1件)：";
    
    //3 商品 下单时间
    _orderTimeLb = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectH(view)-20, CGRectW(view)-20, 15)];
    [view addSubview:_orderTimeLb];
    //_orderTimeLb.backgroundColor = __TestOColor;
    _orderTimeLb.font = MFont(11);
    _orderTimeLb.textColor = HEXCOLOR(0x333333);
    _orderTimeLb.text = @"下单时间：2016-09-16 14:00:00";
    
    //4 边线line3
    UIImageView *lineIV3 = [[UIImageView alloc] initWithFrame:CGRectMake(10, CGRectH(view)-1, CGRectW(view)-10, 1)];
    [view addSubview:lineIV3];
    lineIV3.backgroundColor = HEXCOLOR(0xdedede);
}


- (void)waitReceiveButtonAction:(UIButton *)btn {
    [self.delegate orderWaitReceiveOperating:self.order andButton:btn];
}


//商品添加内容
- (void)addTourismViewInfoDataWithAdultsNum:(NSString *)adultsNum andAdultsPrice:(NSString *)adultsPrice andMinorsNum:(NSString *)minorsNum andMinorsPrice:(NSString *)minorsPrice {
    
    
    
    //控件出来
    _tourismInfoView.hidden = NO;
    _settleCountLb.hidden = YES;
    
    
    float adultsPriceSum;
    float minorsPriceSum;
    adultsPriceSum = [adultsNum intValue] * [adultsPrice floatValue];
    minorsPriceSum = [minorsNum intValue] * [minorsPrice floatValue];
    
    adultsLb.text = [NSString stringWithFormat:@"成人：人数_%@ 单价_￥%@  小计：￥%.2f",adultsNum, adultsPrice, adultsPriceSum];
    minorsLb.text = [NSString stringWithFormat:@"儿童：人数_%@ 单价_￥%@  小计：￥%.2f",minorsNum, minorsPrice, minorsPriceSum];
    
    if (IsNilString(minorsNum) && IsNilString(minorsPrice)) {
        minorsLb.hidden = YES;
    }
    
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
