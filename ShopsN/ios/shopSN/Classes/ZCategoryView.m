//
//  ZCategoryView.m
//  亿速
//
//  Created by chang on 16/6/22.
//  Copyright © 2016年 wshan. All rights reserved.
//
#import "ZCategoryView.h"
#import "ZCategoryMainCell.h"   //左侧 主分类 Cell
#import "ZCategorySubCell.h"    //右侧 子分类 Cell
#import "ZCategorySubHeader.h"  //右侧 子分类 Header
#import "ZCategorySubFooter.h"  //右侧 子分类 Footer

#import "ZGoodsListViewController.h" //产品列表页面

@interface ZCategoryView ()<UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
{
//    NSArray *_categoryArr;       //主类 数组
//    NSArray *_subCategoryArr;    //子类 数组
//    NSArray *_goodsArr;      //产品 数组
    NSInteger _one;//第一列 主类
    NSInteger _two;//第二列 子类
    
    UIView *bgView;
    
}

/** 主分类 列表 */
@property (nonatomic, strong)UITableView *categoryTableView;

/** 子分类 列表 */
@property (nonatomic, strong)UICollectionView *subCategoryCollectionView;

/** 子分类 数组 */
@property (nonatomic, strong) NSMutableArray *subCategorys;



@end

@implementation ZCategoryView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        //
        bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        
        bgView.backgroundColor = HEXCOLOR(0xf0f0f0);
        [self addSubview:bgView];
        
        
  
        
        //[self loadData];
        
        _one = 0;
        _two = 0;
        
        
        
        //添加子视图
        //[self initSubViewWithView:bgView];
        
              
        
    }
    return self;
}

- (void)getDataWithCategoryArray:(NSArray *)categoryArray andRowTitleArray:(NSArray *)rowTitleArray {
    NSLog(@"%d==%d",categoryArray.count, rowTitleArray.count);
    
    _categoryArr = categoryArray;
    _titleArr    = rowTitleArray;
    
    //[bgView removeFromSuperview];
    
    _subCategorys = [NSMutableArray array];
    
    //添加子视图
    [self initSubViewWithView:bgView];
    
}


//- (void)loadData {
//    
//    //测试数据
//    //主类分类名称
//    //热门分类 全球购 奶粉纸尿裤 童装童鞋	 食品  用品玩具  女装内衣   鞋包佩饰  美妆个护 居家百货
//    //_categoryArr = @[@"热门分类",@"全球购",@"奶粉纸尿裤",@"童装童鞋",@"食品",@"用品玩具",@"女装内衣",@"鞋包佩饰",@"美妆个护",@"居家百货"];
//    //_categoryArr = @[@"热门分类",@"奶粉纸尿裤",@"童装童鞋",@"食品",@"用品玩具",@"女装内衣",@"鞋包佩饰",@"美妆个护",@"居家百货"];
//    
//    //子类商品 名称
//    // 套装 连衣裙 T恤 哈衣爬服  外套 亲子装  运动鞋  帆布鞋 凉鞋
//    _titleArr = @[@"套装",@"连衣裙",@"T恤",@"哈衣爬服",@"外套",@"亲子装",@"运动鞋",@"帆布鞋",@"凉鞋"];
//    
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"GoodsList" ofType:@"plist"];
//    _categoryArr = [NSMutableArray arrayWithContentsOfFile:path];
//
//    
//    
//}

- (void)initSubViewWithView:(UIView *)view {
    
    //1 标题
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, CGRectW(view)-20, 39)];
    //title.backgroundColor = HEXCOLOR(0xf0f0f0);
    title.font = MFont(14);
    title.text = @"全部分类";
    title.textColor = HEXCOLOR(0x333333);
    [bgView addSubview:title];
    
    //2 底线
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 40, CGRectW(view), 1)];
    lineIV.backgroundColor = HEXCOLOR(0xdedede);
    [view addSubview:lineIV];
    
    
    //3 主分类 列表
    _categoryTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 41, 90, CGRectH(view)-40)];
    _categoryTableView.backgroundColor = HEXCOLOR(0xf0f0f0);
    _categoryTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _categoryTableView.tag = 1111;
    _categoryTableView.dataSource = self;
    _categoryTableView.delegate   = self;
    [view addSubview:_categoryTableView];
    
    //3 子分类 列表
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    CGRect frame = CGRectMake(CGRectXW(_categoryTableView), 41, CGRectW(view)-CGRectW(_categoryTableView), CGRectH(view)-40);
    _subCategoryCollectionView = [[UICollectionView alloc] initWithFrame:frame collectionViewLayout:flowLayout];
    _subCategoryCollectionView.backgroundColor = HEXCOLOR(0xffffff);
    _subCategoryCollectionView.dataSource = self;
    _subCategoryCollectionView.delegate   = self;
    [view addSubview:_subCategoryCollectionView];
    
    //注册cell和ReusableView
    //1 cell
    [_subCategoryCollectionView registerClass:[ZCategorySubCell class] forCellWithReuseIdentifier:@"SubCell"];
    //2 header
//    [_subCategoryCollectionView registerClass:[ZCategorySubHeader class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"SubHeader"];
//    //3 footer
//    [_subCategoryCollectionView registerClass:[ZCategorySubFooter class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"SubFooter"];
    
    
    
}

#pragma mark - tableView DataSource and Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if (tableView.tag == 1111) {
//        return categoryArr.count;
//    }
    
    return _categoryArr.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = @"CateCell";
    ZCategoryMainCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[ZCategoryMainCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
//        cell.textLabel.font = MFont(14);
//        cell.textLabel.textColor = HEXCOLOR(0x333333);
//        cell.textLabel.text = categoryArr[indexPath.row];
//        cell.categoryMainCellTitleLb.text = categoryArr[indexPath.row];
        
        
        cell.categoryMainCellSideIV.hidden = YES;
        ZCategory *category = [[ZCategory alloc] init];
        category = _categoryArr[indexPath.row];
        cell.categoryMainCellTitleLb.text = category.categoryName;
        //cell.categoryMainCellTitleLb.text = _categoryArr[indexPath.row];
        _one = indexPath.row;
        
    }
    
   
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCategoryMainCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    //cell.backgroundColor = HEXCOLOR(0xffffff);
    cell.categoryMainCellBGView.backgroundColor = HEXCOLOR(0xffffff);
    cell.categoryMainCellSideIV.hidden = NO;
    //NSInteger chooseIndex = indexPath.row;
    //_subCategoryArr = _categoryArr[chooseIndex][@"subCategory"];
    //NSLog(@"chooseIndex:%d",chooseIndex);
    //_subCategoryArr = _categoryArr[indexPath.row];
    //[_subCategoryCollectionView reloadData];
    
    
    NSInteger chooseIndex = indexPath.row;
    NSLog(@"chooseIndex:%d",chooseIndex);
    ZCategory *category = [[ZCategory alloc] init];
    category = _categoryArr[chooseIndex];
    
    
    if (_isGoods) {
        NSLog(@"会员商品");
        [self reloadGoodsCategoryRequestData:category.categoryID];
    }
}



#pragma mark - reloadData
//加载会员商品数据
- (void)reloadGoodsCategoryRequestData:(NSString *)categoryID {
    //115.159.146.202/api/goods.php?action=two_level_class&id=17
    NSMutableArray *mutArr = [NSMutableArray array];
    [ZHttpRequestService POSTGoods:@"Goods/twoClassData" withParameters:@{@"id":categoryID} success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            NSArray *data=jsonDic[@"data"];
            for (NSDictionary *dic in data) {
                
                NSString *className = [Utils getTextStrByText:dic[@"class_name"]];
                [mutArr addObject:className];
                
            }
            
            //NSLog(@"mutArr:%@",mutArr);
            
            
            _subCategorys = [Parse parseSubCategory:jsonDic[@"data"]];
            _titleArr = mutArr;
            [_subCategoryCollectionView reloadData];
            
            
        }else{
            NSString *message = jsonDic[@"message"];
            [SXLoadingView showAlertHUD:message duration:SXLoadingTime];
            
        }
    } failure:^(NSError *error) {
        [SXLoadingView showAlertHUD:@"请求失败" duration:SXLoadingTime];
        
    } animated:NO withView:nil];

}


- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCategoryMainCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    //cell.backgroundColor = HEXCOLOR(0xf0f0f0);
    cell.categoryMainCellBGView.backgroundColor = HEXCOLOR(0xf0f0f0);
    cell.categoryMainCellSideIV.hidden = YES;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45;
}



#pragma mark - collectionView DataSource and Delegate
//- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
//    return _subCategoryArr.count;
//}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _titleArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ZCategorySubCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SubCell" forIndexPath:indexPath];
    
//    NSLog(@"x:%f===%f",cell.frame.origin.x, cell.frame.origin.y);
//    NSLog(@"w:%f===h:%f",cell.frame.size.width, cell.frame.size.height);
    
    //测试数据
    if (indexPath.section == 0) {
        cell.goodsLb.text = _titleArr[indexPath.row];
    }
 
    
    
    return cell;
}

//header和footer
//- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
//    UICollectionReusableView *reusableView = nil;
//    if (kind == UICollectionElementKindSectionHeader) {
//        ZCategorySubHeader *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"SubHeader" forIndexPath:indexPath];
//        
//        //添加数据
////      headerView.titleLb.text = [subCategoryArr[indexPath.row] valueForKey:@"SubCategoryName"]; //应该是section而不是row
//        headerView.titleLb.text = [_subCategoryArr[indexPath.section] valueForKey:@"SubCategoryName"];
//        //headerView.titleLb.text = _subCategoryArr[indexPath.section];
//        
//        
//        reusableView = headerView;
//    }
//    if (kind == UICollectionElementKindSectionFooter) {
//        ZCategorySubFooter *footerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"SubFooter" forIndexPath:indexPath];
//        reusableView = footerView;
//    }
//    
//    return reusableView;
//}

//内容距离屏幕边缘的距离 参数顺序是top,left,bottom,right
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsZero;
    
}


//x 间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

//y 间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 1;
}

//item 宽高
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //横向3 纵向3
//    return CGSizeMake((CGRectW(_subCategoryCollectionView)-3)/3, CGRectW(_subCategoryCollectionView)/3 + 24);
    
    //横向2
    return CGSizeMake((CGRectW(_subCategoryCollectionView)-2)/2, 44);
    
}

////header 宽高
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
//    return CGSizeMake(CGRectW(_subCategoryCollectionView), 45);
//}
//
////footer 宽高
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
//    return CGSizeMake(CGRectW(_subCategoryCollectionView), 5);
//}

//选中触发方法
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"选择的是: 第%ld组 第%ld个",(long)indexPath.section, (long)indexPath.row);
    
//    //跳转 页面
//    BaseNavigationController *navi = (BaseNavigationController *)[UIApplication sharedApplication].keyWindow.rootViewController;
//    ZSiftViewController *vc = [[ZSiftViewController alloc] init];
//    vc.siftName = goodsArr[indexPath.row];
//    [navi pushViewController:vc animated:YES];
//    NSString *goodsClassName = _titleArr[indexPath.row];
//    [self.delegate pushToGoodsListVCWithName:goodsClassName withIndexPath:indexPath];
    
    
    NSInteger chooseIndex = indexPath.row;
    ZSubCategory *subCategory = [[ZSubCategory alloc] init];
    subCategory = _subCategorys[chooseIndex];
    
    
    
    [self.delegate pushToGoodsListVCwithSubCategoryID:subCategory.subCategoryID andSubCategoryName:subCategory.subCategoryName];
    
}


#pragma mark-- 设置默认选中内容
- (void)defaultChoose:(NSArray *)array {
    
    //如果有数据，默认选中第一行并请求第一行的数据
    if (array.count > 0) {
        NSIndexPath *first = [NSIndexPath indexPathForRow:0 inSection:0];
        [_categoryTableView selectRowAtIndexPath:first animated:YES scrollPosition:UITableViewScrollPositionTop];
        [self tableView:_categoryTableView didSelectRowAtIndexPath:first];
        
    }
}





/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
