//
//  ZCAccountBalanceViewController.m
//  shopSN
//
//  Created by chang on 16/7/10.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCAccountBalanceViewController.h"
#import "ZAccountManagerModel.h"
@interface ZCAccountBalanceViewController ()

/** 余额label */
@property (nonatomic, strong) UILabel *balanceLb;

/** 积分label */
@property (nonatomic, strong) UILabel *pointsLb;

@end

@implementation ZCAccountBalanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLb.text = @"余额";
    
    //初始化 中间视图 相关子视图color(239 240 241) (0xeff0f1)
    self.mainMiddleView.backgroundColor = __AccountBGColor;
    [self initSubViews:self.mainMiddleView];
}


#pragma mark - 中间视图部分 相关子视图
//初始化相关子视图
- (void)initSubViews:(UIView *)view {
    //单例取值
    NSDictionary *dicData = [ZAccountManagerModel shareAccountManagerData].accountDic;

    //背景视图
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(view), 210)];
    [view addSubview:bgView];
    bgView.backgroundColor = HEXCOLOR(0xffffff);
    
    //1 按钮视图 color(253 247 231) (0xfdf7e7)
    UIView *btnView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(bgView), 25)];
    [bgView addSubview:btnView];
    btnView.backgroundColor = HEXCOLOR(0xfdf7e7);
    [self addBtnViewSubViews:btnView];
    // button
    UIButton *shoppingBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, CGRectW(bgView), 25)];
    [bgView addSubview:shoppingBtn];
    [shoppingBtn addTarget:self action:@selector(shoppingBtnAction) forControlEvents:BtnTouchUpInside];
    
    
    //2 标题label1  钱包余额（元）
    UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectYH(btnView)+15, CGRectW(bgView)-20, 15)];
    [bgView addSubview:titleLb];
    titleLb.font = MFont(12);
    titleLb.textColor = HEXCOLOR(0x999999);
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.text = @"钱包余额（元）";
    
    //3 余额label
    _balanceLb = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectYH(titleLb)+10, CGRectW(bgView)-20, 40)];
    [bgView addSubview:_balanceLb];
//    _balanceLb.backgroundColor = __TestOColor;
    _balanceLb.font = [UIFont fontWithName:@"Helvetica-Bold" size:45];//文字加粗
    _balanceLb.textColor = __DefaultColor;
    _balanceLb.textAlignment = NSTextAlignmentCenter;
    _balanceLb.text = dicData[@"account_balance"];
    
    
    //4 标题label2  积分
    for (int idx = 0; idx<2; idx++) {
        UILabel *titleLb2 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(bgView)/2*idx, CGRectYH(_balanceLb)+30, CGRectW(bgView)/2, 15)];
        [bgView addSubview:titleLb2];
        titleLb2.font = MFont(12);
        titleLb2.textColor = [UIColor blackColor];
        titleLb2.textAlignment = NSTextAlignmentCenter;
        titleLb2.text = @"积分";
    }
    UILabel *titleLb2 = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectYH(_balanceLb)+30, CGRectW(bgView)-20, 15)];
    
    
    //5 积分label
    
    NSArray *arr = @[dicData[@"add_jf_limit"],dicData[@"add_jf_currency"]];
    
    _pointsLb = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectYH(titleLb2)+5, CGRectW(bgView)/2, 30)];
    [bgView addSubview:_pointsLb];
//    _pointsLb.backgroundColor = __TestOColor;
    _pointsLb.font = [UIFont fontWithName:@"Helvetica-Bold" size:30];//文字加粗
    _pointsLb.textColor = [UIColor blackColor];
    _pointsLb.textAlignment = NSTextAlignmentCenter;
    _pointsLb.text = arr[0];
    
    UILabel *BLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_pointsLb), CGRectYH(titleLb2)+5, CGRectW(bgView)/2, 30)];
    BLabel.font = _pointsLb.font;
    BLabel.textAlignment = 1;
    BLabel.text = arr[1];
    [bgView addSubview:BLabel];
    
    
    
    //底部 充值按钮按钮
    UIButton *topupBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectH(view)-45, CGRectW(view), 45)];
    [view addSubview:topupBtn];
    topupBtn.backgroundColor = __DefaultColor;
    topupBtn.titleLabel.font = MFont(14);
    [topupBtn setTitle:@"充值" forState:BtnNormal];
    [topupBtn setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
    [topupBtn addTarget:self action:@selector(topupBtnAction) forControlEvents:BtnTouchUpInside];
    
}

//按钮视图 添加子控件
- (void)addBtnViewSubViews:(UIView *)view {
    //1.1 label color(252 183 85) (0xfcb755)
    //          钱包余额可在shopSN商城购物时使用，不可提现
    UILabel *btnTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, CGRectW(view)-20-30, 15)];
    [view addSubview:btnTitle];
    btnTitle.font = MFont(12);
    btnTitle.textColor = HEXCOLOR(0xfcb755);
    btnTitle.text = [NSString stringWithFormat:@"钱包余额可在%@商城购物时使用，不可提现",simpleTitle];
    //1.2 imageView
    UIImageView *btnIV = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectW(view)-10-15, 5, 9, 15)];
    [view addSubview:btnIV];
//    btnIV.backgroundColor = HEXCOLOR(0xfcb755);
    btnIV.image =MImage(@"jt_h.png");
}


#pragma mark - ==== 按钮触发方法 =====
- (void)shoppingBtnAction {
    NSLog(@"前往商城购物");
}


- (void)topupBtnAction {
    NSLog(@"跳转至 充值页面");
}

#pragma mark - ==== others =====
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
