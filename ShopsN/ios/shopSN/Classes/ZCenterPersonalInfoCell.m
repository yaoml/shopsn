//
//  ZCenterPersonalInfoCell.m
//  shopSN
//
//  Created by chang on 16/7/17.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCenterPersonalInfoCell.h"

@implementation ZCenterPersonalInfoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _titleLb = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 100, 44)];
        _titleLb.font = MFont(12);
        _titleLb.textColor = HEXCOLOR(0x565656);
        [self addSubview:_titleLb];
        
        
        _titleIV = [[UIImageView alloc] initWithFrame:CGRectMake(__kWidth-30-100, 2, 100, 36)];
        
        [self addSubview:_titleIV];
        
        _lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 43, __kWidth, 1)];
        _lineIV.backgroundColor = HEXCOLOR(0xdedede);
        [self addSubview:_lineIV];
    }
    return self;
}



@end
