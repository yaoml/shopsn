//
//  ZCMyOrderClosePayFooter.h
//  shopSN
//
//  Created by yisu on 16/7/14.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 我的模块 我的订单页面
 *
 *  交易关闭 footer
 *
 */
#import "BaseView.h"

@interface ZCMyOrderClosePayFooter : BaseView

/** 交易关闭订单 商品数量 */
@property (nonatomic, strong) UILabel *cp_countLb;


/** 交易关闭订单 商品数量 */
@property (nonatomic, assign) NSInteger cp_shopCount;


/** 交易关闭订单 价格 */
@property (nonatomic, strong) UILabel *cp_settleNowPriceLb;

/** 交易关闭订单 数量 */
@property (nonatomic, strong) UILabel *cp_settleGoodsCountLb;


/** 交易关闭订单 删除订单 按钮*/
@property (nonatomic, strong) UIButton *cp_deleteOrderBtn;

@end
