//
//  ZCAccountDiscountViewController.m
//  shopSN
//
//  Created by chang on 16/7/10.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCAccountDiscountViewController.h"
#import "ZCAccountDiscountModel.h"
#import "ZCAccountDiscountCell.h"

@interface ZCAccountDiscountViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    UITableView *_tableView;
}

//优惠券数组
@property (nonatomic, strong) NSArray *discountNumArray;


@end

@implementation ZCAccountDiscountViewController



#pragma mark - ==== 页面设置 =====
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLb.text = @"优惠券";
    
    //初始化 中间视图 相关子视图
    self.mainMiddleView.backgroundColor = __AccountBGColor;
    [self initSubViews:self.mainMiddleView];
}


#pragma mark - 中间视图部分 相关子视图
//初始化相关子视图
- (void)initSubViews:(UIView *)view {
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(10, 35, CGRectW(view)-20, CGRectH(view)-35-50)];
    [view addSubview:_tableView];
//    _tableView.backgroundColor = __TestOColor;
    _tableView.backgroundColor = __AccountBGColor;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.dataSource = self;
    _tableView.delegate   = self;
    
}


#pragma mark- ===== tableView DataSource and Delegate =====
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCAccountDiscountCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DiscountCell"];
    if (!cell) {
        cell = [[ZCAccountDiscountCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DiscountCell"];
        
    }
    
    /**
        test
        满199元可用   2016.06.16 -2017-06.15
     */
    
    cell.ca_discountMoneyLb.text = @"100";
    cell.ca_discountReference.text = @"满199元可用";
    cell.ca_discountTime.text = @"2016.06.16--2017.06.15";
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 155;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

#pragma mark - ==== others =====
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
