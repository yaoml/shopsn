//
//  ZCenterAccountViewController.m
//  shopSN
//
//  Created by chang on 16/7/9.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCenterAccountViewController.h"
#import "ZCAccountPointsViewController.h"   //积分页面
#import "ZCAccountBalanceViewController.h"  //余额页面
#import "ZCAccountDiscountViewController.h" //优惠券页面
#import "ZCAccountBankCardViewController.h" //我的银行卡页面

#import "ZAccountManagerModel.h"

#import "ZCenterAccountCell.h"

@interface ZCenterAccountViewController ()<UITableViewDataSource, UITableViewDelegate>

{
    UITableView *_tableView;
}

/**
 *  图片数组
 */
@property (nonatomic ,strong)NSArray *iconArray;

/**
 *  标题数组
 */
@property (nonatomic ,strong)NSArray *titleArray;
/**
 *  详情数组
 */
@property (nonatomic ,strong)NSArray *detailArray;



@end

@implementation ZCenterAccountViewController

#pragma mark - ==== 懒加载 =====
/*
   账户余额 银行卡  我的积分  优惠券
   ￥90.00  无 20分  6张
 */
- (NSArray *)iconArray {
    if (!_iconArray) {
        _iconArray = [NSArray array];
        _iconArray = @[@"zh.png", @"yhk.png", @"jf.png", @"quan.png"];
    }
    return _iconArray;
}

- (NSArray *)titleArray {
    if (!_titleArray) {
        _titleArray = [NSArray array];
        _titleArray = @[@"账户余额", @"银行卡", @"我的积分", @"优惠券"];
    }
    return _titleArray;
}

- (NSArray *)detailArray {
    if (!_detailArray) {
        _detailArray = [NSArray array];
        
        _detailArray = @[@"¥0.0", @"无", @"0分", @"0张"];
    }
    return _detailArray;
}


#pragma mark - ==== 页面设置 =====

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"账户管理";
    
    //视图 初始化
    [self initViews];
    [self getAccountManagerData];
}
#pragma mark *** 获取账户管理数据 ***
-(void)getAccountManagerData{
    NSDictionary *params =@{@"token":[UdStorage getObjectforKey:Userid],
                           };
    [ZHttpRequestService POST:@"User/personManage" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
      
            NSArray *data = jsonDic[@"data"];
            NSDictionary *dic = data[0];
            [ZAccountManagerModel shareAccountManagerData].accountDic = dic;
            
            _detailArray = @[dic[@"account_balance"],[dic[@"num"] isEqualToString:@"0"]?@"无":[NSString stringWithFormat:@"%@张",dic[@"num"]],dic[@"integral"],@"0张"];
            [_tableView reloadData];
        }else{
            NSString *message = jsonDic[@"message"];
            [SXLoadingView showAlertHUD:message duration:2];
        }
        
        
    } failure:^(NSError *error) {
        
        [SXLoadingView showAlertHUD:@"失败" duration:SXLoadingTime];
        
    } animated:true withView:nil];
}
//视图 初始化
- (void)initViews {
    
    
    //顶部边线
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 64, __kWidth, 1)];
    [self.view addSubview:lineIV];
    lineIV.backgroundColor = HEXCOLOR(0xdedede);
    
    //背景色 color(239 240 241) (0xeff0f1)
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 65, __kWidth, __kHeight-65)];
    [self.view addSubview:_tableView];
        _tableView.backgroundColor = __AccountBGColor;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.dataSource = self;
    _tableView.delegate   = self;
    
}


#pragma mark- ===== tableView DataSource and Delegate =====
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.titleArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCenterAccountCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CenterAccountCell"];
    if (!cell) {
        cell = [[ZCenterAccountCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CenterAccountCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
   
    
    cell.ca_iconIV.image = MImage(self.iconArray[indexPath.row]);
    cell.ca_titleLb.text = self.titleArray[indexPath.row];
    cell.ca_detailLb.text = self.detailArray[indexPath.row];
    
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
        {
            NSLog(@"进入 余额页面");
            ZCAccountBalanceViewController *vc = [[ZCAccountBalanceViewController alloc] init];
            vc.myBalanceStr = @"90.00";
            vc.myPointsStr  = @"20";
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        case 1:
        {
            NSLog(@"进入 银行卡页面");
            ZCAccountBankCardViewController *vc = [[ZCAccountBankCardViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        case 2:
        {
            NSLog(@"进入 积分页面");
            ZCAccountPointsViewController *vc = [[ZCAccountPointsViewController alloc] init];
            vc.myPointsStr = @"1234";
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        case 3:
        {
            NSLog(@"进入 优惠券页面");
            ZCAccountDiscountViewController *vc = [[ZCAccountDiscountViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        default:
            break;
    }
    
}


#pragma mark - ==== others =====
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
