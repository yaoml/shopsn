//
//  ZHomePicModel.h
//  shopSN
//
//  Created by yisu on 16/9/19.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 会员商品 首页页面
 *
 *  数据 模型类
 *
 */

#import <Foundation/Foundation.h>

@interface ZHomePicModel : NSObject

//数据数组
///** 首页广告图片 数组 */
//@property (nonatomic, strong) NSArray *bannerArray;
//
///** 首页中心图片 数组 */
//@property (nonatomic, strong) NSArray *centerArray;
//
///** 首页热销商品 数组 */
//@property (nonatomic, strong) NSArray *hotArray;
//
//
///** 首页分类商品 数组 */
//@property (nonatomic, strong) NSArray *classArray;





//数据内容
/** 首页banner 图片url */
@property (copy, nonatomic) NSString *bannerUrl;

/** 首页hot 图片url */
@property (copy, nonatomic) NSString *hotUrl;

/** 首页hotID */
@property (copy, nonatomic) NSString *hotID;

/** 首页center 图片url */
@property (copy, nonatomic) NSString *centerUrl;

/** 首页 图片链接url */
@property (copy, nonatomic) NSString *linkUrl;


/** 首页class 图片url */
@property (copy, nonatomic) NSString *classUrl;


/** 首页class id */
@property (copy, nonatomic) NSString *classID;

/** 首页class name */
@property (copy, nonatomic) NSString *className;

@end
