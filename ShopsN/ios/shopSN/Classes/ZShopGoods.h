//
//  ZShopGoods.h
//  shopSN
//
//  Created by yisu on 16/7/1.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 购物车页面
 *
 *  店铺商品模型
 *
 */
#import <Foundation/Foundation.h>

@interface ZShopGoods : NSObject

/** 购物车 店铺商品 是否选中 */
@property (nonatomic, assign) BOOL goodsCheck;

/** 购物车 店铺商品编辑 是否隐藏 */
@property (nonatomic, assign) BOOL goodsEditHidden;

@end
