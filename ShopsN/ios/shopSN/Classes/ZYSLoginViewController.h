//
//  ZYSLoginViewController.h
//  shopSN
//
//  Created by imac on 2016/10/24.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "BaseViewController.h"

@interface ZYSLoginViewController : BaseViewController

/** 登录成功 进入的新页面名称 */
@property (copy, nonatomic) NSString *pageName;
@end
