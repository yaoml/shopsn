//
//  ZCommonGoodsHeader.m
//  shopSN
//
//  Created by yisu on 16/9/17.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCommonGoodsHeader.h"
#import "ZHeadImage.h"
#import "ZHeaderScrollViewController.h"

@interface ZCommonGoodsHeader ()
{
    UIView *bgView;
    NSArray *_chooseHeaderImageArray;
}

/** 首页表格header中的顶部滚动视图 */
@property (nonatomic, strong) ZHeaderScrollViewController *headerScrollViewController;

@end



@implementation ZCommonGoodsHeader


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self addSubview:bgView];
        //bgView.backgroundColor = __TestGColor;
        
        
        
        
        //[self initSubViews:bgView];
        
    }
    return self;
}

- (void)getDataWithImageArray:(NSArray *)headerImageArray {
    
    
    // 获取广告图片
    [self addHeaderImage:headerImageArray];
    
    //移除上次加载的 滚动广告视图和专区选择视图
    [self.headerScrollViewController.view removeFromSuperview];
   
    
    
    [self initSubViews:bgView];
    
}


//加载广告图片
- (void)addHeaderImage:(NSArray *)array {
    NSMutableArray *mutArray = [NSMutableArray array];
    for (int i=0; i<array.count; i++) {
        ZHeadImage *headImage = [[ZHeadImage alloc] init];
        headImage.imageName = array[i];
        [mutArray addObject:headImage];
    }
    _chooseHeaderImageArray = mutArray;
    
    
    
}


//添加子视图
- (void)initSubViews:(UIView *)view {
    
    //滚动广告视图
    _headerScrollViewController = [[ZHeaderScrollViewController alloc] init];
    self.headerScrollViewController.headerImageArray = _chooseHeaderImageArray;
    self.headerScrollViewController.picUrl = GoodsImageUrl;
    CGFloat width  = view.frame.size.width;
    CGFloat height = 350;//调整滚动视图图片高度
    
    self.headerScrollViewController.contentSize = CGSizeMake(width, height);
    self.headerScrollViewController.view.frame = CGRectMake(GAP_SPACE, GAP_SPACE, width, height);
    [view addSubview:self.headerScrollViewController.view];
    
    
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
