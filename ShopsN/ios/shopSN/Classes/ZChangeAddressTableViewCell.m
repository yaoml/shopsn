//
//  ZChangeAddressTableViewCell.m
//  shopSN
//
//  Created by imac on 16/7/8.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZChangeAddressTableViewCell.h"

@implementation ZChangeAddressTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style  reuseIdentifier:reuseIdentifier]) {
        [self initView];
    }
    return self;
}

-(void)initView{
    UIView *backV = [[UIView alloc]initWithFrame:CGRectMake(0, 0, __kWidth, 50)];
    [self addSubview:backV];
    backV.backgroundColor = [UIColor whiteColor];
    
    _titleLb = [[UILabel alloc]initWithFrame:CGRectMake(15, 10, 90, 30)];
    [backV addSubview:_titleLb];
    _titleLb.textAlignment = NSTextAlignmentLeft;
    _titleLb.font = MFont(15);
      
    _detailTV= [[UITextField alloc]initWithFrame:CGRectMake(95, 10, __kWidth-130, 30)];
    [backV addSubview:_detailTV];
//    _detailTV.backgroundColor = __TestGColor;
    _detailTV.font = MFont(15);
    
    _chooseIV = [[UIImageView alloc]initWithFrame:CGRectMake(__kWidth-35, 15, 20, 20)];
    [backV addSubview:_chooseIV];
    _chooseIV.layer.cornerRadius = 10;
    _chooseIV.layer.masksToBounds = YES;
    _chooseIV.hidden = YES;
    
//    _addressTF = [[UITextView alloc]initWithFrame:CGRectMake(95, 10, __kWidth-130, 60)];
//    [backV addSubview:_addressTF];
//    _addressTF.backgroundColor = [UIColor whiteColor];
//    _addressTF.font = MFont(15);
//    _addressTF.hidden = YES;
    
    UIImageView *bottomLineIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, CGRectH(backV)-1, __kWidth, 1)];
    [backV addSubview:bottomLineIV];
    bottomLineIV.backgroundColor = HEXCOLOR(0xdedede);
}


@end
