//
//  ZShoppingCarCell.h
//  shopSN
//
//  Created by yisu on 16/7/1.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "BaseTableViewCell.h"

@class ZShoppingCarCell;
@protocol ZShoppingCarCellDelegate <NSObject>
/**
 *  勾选商品
 *
 *  @param cell   ShoppingCarCell
 *  @param _check 勾选状态
 */
- (void)shoppingCarCell:(ZShoppingCarCell *)cell didClicSelectButton:(UIButton *)sender;

/**
 *  删除商品
 *
 *  @param cell ShoppingCarCell
 */
-(void)shoppingCarCellDidSelectDeleteGoods:(ZShoppingCarCell *)cell;

/**
 *  商品增加/减少
 *
 *  @param isAdd
 */
- (void)shoppingCarCell:(ZShoppingCarCell *)cell ChangeGoodsCount:(NSInteger)count;

@end

@interface ZShoppingCarCell : BaseTableViewCell

/** 商品id */
@property (nonatomic, copy) NSString *goodID;

/**购物车商品订单id*/
@property (nonatomic,copy) NSString *cartID;

/** 商品图片 */
@property (nonatomic, strong) UIImageView *sg_imageView;

/** 商品描述 */
@property (nonatomic, strong) UILabel *sg_descLb;

/** 商品原价 */
@property (nonatomic, strong) UILabel *sg_originalPriceLb;

/** 商品现价 */
@property (nonatomic, strong) UILabel *sg_nowPriceLb;

/** 商品数量 */
@property (nonatomic, strong) UILabel *sg_countLb;

/**是套餐名字*/
@property (nonatomic,strong) UILabel *cellIsSet;
/**返积分*/
@property (nonatomic,strong) UILabel *cellReturnPoint;




/** 购物车 选择按钮 */
@property (nonatomic, strong) UIButton *shoppingCarCheckButton;

/** 购物车中商品数量 */
@property (nonatomic, assign) NSInteger sg_shopCount;


/** 结算商品 价格 */
@property (nonatomic, strong) UILabel *sg_settleNowPriceLb;

/** 结算商品 数量 */
@property (nonatomic, strong) UILabel *sg_settleGoodsCountLb;


@property (nonatomic, weak) id<ZShoppingCarCellDelegate>delegate;


@end
