//
//  ZChangePersonIDViewController.h
//  shopSN
//
//  Created by imac on 16/7/7.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 我的模块 个人信息子页面
 *
 *  修改身份证号 视图控制器
 *
 */
#import "BaseViewController.h"

@interface ZChangePersonIDViewController : BaseViewController

@property (strong,nonatomic) NSString *personID;

@end
