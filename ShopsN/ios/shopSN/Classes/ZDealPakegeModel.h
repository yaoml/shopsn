//
//  ZDealPakegeModel.h
//  shopSN
//
//  Created by yisu on 16/9/24.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 商品简介 相关页面
 *
 *  商品套餐 模型类
 *
 */
#import <UIKit/UIKit.h>

@interface ZDealPakegeModel : UIView



/** 旅游商品详情 旅游商品出发日期 */
@property (copy, nonatomic) NSString *tourismStartDate;

/** 旅游商品详情 旅游商品出发时间 成人价格 */
@property (copy, nonatomic) NSString *tourismDatePrice;

/** 旅游商品详情 旅游商品出发时间 成人价格 */
@property (copy, nonatomic) NSString *tourismDateChildPrice;


@end
