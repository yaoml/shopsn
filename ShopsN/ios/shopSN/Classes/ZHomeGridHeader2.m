//
//  ZHomeGridHeader2.m
//  shopSN
//
//  Created by yisu on 16/8/11.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZHomeGridHeader2.h"

@implementation ZHomeGridHeader2

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self addSubview:bgView];
        bgView.backgroundColor = HEXCOLOR(0xffffff);
        
        [self initSubViews:bgView];
    }
    return self;
}



- (void)initSubViews:(UIView *)bgView {
    
    //标题label
    _commonGoodsTitleLb = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 100, 20)];
    [bgView addSubview:_commonGoodsTitleLb];
//    _commonGoodsTitleLb.backgroundColor = __TestGColor;
    _commonGoodsTitleLb.font = MFont(12);
    _commonGoodsTitleLb.textColor = HEXCOLOR(0x333333);
    
    
    //更多button
    UIView *moreView = [[UIView alloc] initWithFrame:CGRectMake(CGRectW(bgView)-60, 5, 50, 20)];
    //moreView.backgroundColor = __TestOColor;
    [bgView addSubview:moreView];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(1, 1, 30, 15)];
    label.text = @"更多";
    label.font = MFont(11);
    label.textColor = HEXCOLOR(0x333333);
    label.textAlignment = NSTextAlignmentRight;
    [moreView addSubview:label];
    
    UIImageView *arrowIV = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectW(moreView)-12, 2, 12, 12)];
    arrowIV.image = MImage(@"jiantou");
    [moreView addSubview:arrowIV];
    
    _commonGoodsMoreBtn = [[UIButton alloc] initWithFrame:bgView.frame];
    [bgView addSubview:_commonGoodsMoreBtn];
    
    
    //底线
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectH(bgView), CGRectW(bgView), 1)];
    [bgView addSubview:lineIV];
    lineIV.backgroundColor = HEXCOLOR(0xdedede);
    
}
@end
