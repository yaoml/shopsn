//
//  ZCodeLoginViewController.m
//  shopSN
//
//  Created by yisu on 16/6/13.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCodeLoginViewController.h"

@interface ZCodeLoginViewController ()

/** 手机号TF */
@property (nonatomic, strong) UITextField *phoneTF;
/** 验证码TF*/
@property (nonatomic, strong) UITextField *codeTF;
/** 验证码Btn */
@property (nonatomic, strong) UIButton *codeBtn;

@end

@implementation ZCodeLoginViewController

#pragma mark - 隐藏标签栏
- (void)viewWillAppear:(BOOL)animated {
    self.tabBarController.tabBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    self.tabBarController.tabBar.hidden = NO;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = __AccountBGColor;
    
    self.title =[NSString stringWithFormat:@"登录%@",simpleTitle] ;
    
    [self initView];//初始化视图
}

#pragma mark - ===== 视图初始化 =====
//初始化视图
- (void)initView {
    //mainView
    UIView *mainView = [[UIView alloc] initWithFrame:CGRectMake(0, 64+8, __kWidth, 88)];
    mainView.backgroundColor = HEXCOLOR(0xffffff);
    [self.view addSubview:mainView];
    
    //1 subView
    for (int i=0; i<2; i++) {
        // (图片 文本框 底线 忘记密码按钮)
        UIView *subView = [[UIView alloc] initWithFrame:CGRectMake(15, 44*i, __kWidth-30, 44)];
        [mainView addSubview:subView];
        
        //图片
        UIImageView *titleIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 11, 22, 22)];
        //titleIV.image = i?MImage(@""):MImage(@"");
        titleIV.backgroundColor = [UIColor orangeColor];//等待切图
        [subView addSubview:titleIV];
        
        //文本框
        UITextField *textField = [[UITextField alloc] initWithFrame:CGRectZero];
        textField.font = MFont(14);
        textField.textColor = HEXCOLOR(0x333333);
        [subView addSubview:textField];
        
        if (i) {
            //忘记密码 按钮
            _codeBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectW(subView)-90, 8, 90, 28)]; //添加余量6
            _codeBtn.layer.borderWidth = 1;
            _codeBtn.layer.borderColor = __DefaultColor.CGColor;
            _codeBtn.layer.cornerRadius = 13;
            _codeBtn.titleLabel.font = MFont(14);
            [_codeBtn setTitle:@"获取验证码" forState:BtnNormal];
            [_codeBtn setTitleColor:__DefaultColor forState:BtnNormal];
            [_codeBtn addTarget:self action:@selector(getCodeLoginAction) forControlEvents:BtnTouchUpInside];
            [subView addSubview:_codeBtn];
            
            //输入密码 文本框
            textField.frame = CGRectMake(CGRectXW(titleIV)+10, 0, CGRectX(_codeBtn)-CGRectXW(titleIV)-20, 44);
            _codeTF = textField;
            _codeTF.placeholder = @"请输入短信验证码";
            //_passwordTF.backgroundColor = __TestGColor;
        }else {
            textField.frame = CGRectMake(CGRectXW(titleIV)+10, 0, CGRectW(subView)-CGRectXW(titleIV)-10, 44);
            _phoneTF = textField;
            _phoneTF.placeholder = @"请输入11位手机号码";
            //_phoneTF.backgroundColor = __TestOColor;
        }
        
    }
    
    //2 中间line
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(48, 44, __kWidth-48, 1)];
    lineIV.backgroundColor = HEXCOLOR(0xd8d8d8);
    [mainView addSubview:lineIV];
    
    
    //3 登录按钮
    UIButton *codeLoginBtn = [[UIButton alloc] initWithFrame:CGRectMake(15, CGRectYH(mainView)+15, __kWidth-30, 44)];
    codeLoginBtn.backgroundColor = __DefaultColor;
    codeLoginBtn.titleLabel.font = MFont(16);
    codeLoginBtn.layer.cornerRadius = 5;
    [codeLoginBtn setTitle:@"立即登录" forState:BtnNormal];
    [codeLoginBtn setTitleColor:HEXCOLOR(0xffffff) forState:BtnNormal];
    [codeLoginBtn addTarget:self action:@selector(codeLoginAction) forControlEvents:BtnTouchUpInside];
    [self.view addSubview:codeLoginBtn];

}


#pragma mark - ===== 按钮方法 =====
//获取手机验证码
- (void)getCodeLoginAction {
    NSLog(@"获取手机验证码");
    
}

- (void)codeLoginAction {
    NSLog(@"立即登录");
}



#pragma mark - ===== 移除键盘 =====
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (_phoneTF || _codeTF) {
        [self.view endEditing:YES];
    }
}

#pragma mark - others
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
