//
//  ZPayPointsCell.m
//  shopSN
//
//  Created by chang on 16/7/4.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZPayPointsCell.h"

@interface ZPayPointsCell ()

/** 积分选择 按钮*/
@property (nonatomic, strong) UIButton *payPointsCheckButton;



@end

@implementation ZPayPointsCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
   
        NSArray *title = @[@"使用A积分账户：",@"使用B积分账户：",@"账户积分余额：",@"运费：¥0",@"温馨提示：积分兑换商品至少支付1元人民币。"];
        
        for (int idx = 0; idx<title.count; idx++) {
            UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(10, 14+38*idx, 100, 30)];
            
            [self addSubview:titleLb];
            titleLb.font = MFont(12);
            titleLb.textColor = HEXCOLOR(0x333333);
            titleLb.textAlignment = 0;
            titleLb.text = title[idx];
            
            if (idx==4) {
                titleLb.frame = CGRectMake(10, 14+38*idx, 350, 30);
                titleLb.textColor = [UIColor redColor];
            }
            if (idx==0) {
                _pointATX = [[UITextField alloc] initWithFrame:CGRectMake(CGRectXW(titleLb), CGRectY(titleLb), 100, 30)];
                _pointATX.layer.borderWidth = 1;
                _pointATX.layer.borderColor = LH_RGBCOLOR(230, 230, 230).CGColor;
                _pointATX.font = MFont(13);
                _pointATX.text = @"0";
//                _pointATX.keyboardType = UIKeyboardTypeNumberPad;
                [self addSubview:_pointATX];
            }else if (idx==1){
                _pointBTX = [[UITextField alloc] initWithFrame:CGRectMake(CGRectXW(titleLb), CGRectY(titleLb), 100, 30)];
                _pointBTX.layer.borderWidth = 1;
                _pointBTX.layer.borderColor = LH_RGBCOLOR(230, 230, 230).CGColor;
                _pointBTX.font = MFont(13);
                 _pointBTX.text = @"0";
                
//                _pointBTX.keyboardType = UIKeyboardTypeNumberPad;
                [self addSubview:_pointBTX];
            }else if (idx==2){
                _pointALabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(titleLb), CGRectY(titleLb), 100, 30)];
                _pointALabel.textColor = LH_RGBCOLOR(252, 97, 77);
                _pointALabel.font = MFont(12);
                _pointALabel.text = @"A账户：0.00000";
                [self addSubview:_pointALabel];
                _pointBLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_pointALabel)+10, CGRectY(titleLb), 100, 30)];
                _pointBLabel.font = MFont(12);
                _pointBLabel.textColor = _pointALabel.textColor;
                _pointBLabel.text = @"A账户：0.00000";
                [self addSubview:_pointBLabel];
            }
            
            
        }

    }
    return self;
}


- (void)setLabeltextAttributes:(UILabel *)label {
    NSString *text = label.text;
    NSUInteger length = [text length];
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:text];
    [attri addAttribute:NSForegroundColorAttributeName value:__MoneyColor range:NSMakeRange(7, length-7)];
    
    [label setAttributedText:attri];
}



@end
