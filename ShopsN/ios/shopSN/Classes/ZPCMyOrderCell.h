//
//  ZPCMyOrderCell.h
//  shopSN
//
//  Created by chang on 16/7/7.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 我的模块 主页面
 *
 *  我的订单 cell
 *
 */
#import "BaseTableViewCell.h"

@class ZPCMyOrderCell;
@protocol ZPCMyOrderCellDelegate <NSObject>

- (void)didOrderButton:(UIButton *)sender;

@end

@interface ZPCMyOrderCell : BaseTableViewCell

/***/
@property (weak, nonatomic) id<ZPCMyOrderCellDelegate>delegate;

@end
