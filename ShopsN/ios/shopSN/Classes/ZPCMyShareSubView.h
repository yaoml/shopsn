//
//  ZPCMyShareSubView.h
//  shopSN
//
//  Created by chang on 16/7/7.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 我的分享 cell
 *
 *  子视图
 *
 */
#import "BaseView.h"

@interface ZPCMyShareSubView : BaseView



/** 标题label */
@property (nonatomic, strong) UILabel *titleLb;


/** 内容label */
@property (nonatomic, strong) UILabel *descLb;

@end
