//
//  ZPayMoney.m
//  shopSN
//
//  Created by 王子豪 on 16/9/30.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZPayMoney.h"
#import "UPPaymentControl.h"
#import "WXApi.h"
@implementation ZPayMoney
+(void)payWithOrderNumber:(NSString *)orderNum title:(NSString *)title price:(NSString *)price complete:(void (^)())back{
    
    
    [ZHttpRequestService POST:@"PayOrder/"
               withParameters:@{@"token":[UdStorage getObjectforKey:Userid],
                                                            @"orders_num":orderNum,
                                                             @"goods_title":title,
                                                             @"price_shiji":price} success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
                                                                 
                                                                 if (succe) {
                                                                     [self alipayWithorderStr:jsonDic[@"data"] complete:nil];
                                                                     back();
                                                                 }
                                                                 
                                                             } failure:^(NSError *error) {
                                                                 
                                                                 back();
                                                             } animated:YES withView:nil];
    
}
//银联支付
+(void)UnionPayWithOrderNumber:(NSString *)orderNum price:(NSString *)price withVc:(UIViewController *)target complete:(void (^)())back{
    [ZHttpRequestService POST:@"PayOrder/ylPay" withParameters:@{@"token":[UdStorage getObjectforKey:Userid],
                                                                 @"orders_num":orderNum,
                                                         @"price_shiji":price} success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
                                                             if (succe) {
                                                                 NSString *TnNum = jsonDic[@"data"][@"tn"];
                                                                 [self UnionPayWithTn:TnNum withVc:target];
//                                                                 back();
                                                             }
                                                         } failure:^(NSError *error) {
                          
                                                         } animated:YES withView:nil];
}

#pragma mark *** 支付 ***
//支付宝
+(void)alipayWithorderStr:(NSString *)orderStr complete:(void (^)())back{
    
    //应用注册scheme,在AlixPayDemo-Info.plist定义URL types
    NSString *appScheme = AlipayScheme;
    
    NSLog(@"orderstr---%@",orderStr);
    //获取的orderstr直接调用支付宝
    [[AlipaySDK defaultService] payOrder:orderStr fromScheme:appScheme callback:^(NSDictionary *resultDic) {
        NSLog(@"reslut = %@",resultDic);
        
        //调用后推到首页
//        [self.navigationController popToRootViewControllerAnimated:false];
//        self.tabBarController.selectedIndex = 0;
//        back();
        
    }];
}
//微信

+(void)weiXinPayWithOrderNumber:(NSString *)orderNum title:(NSString *)title price:(NSString *)price complete:(void (^)())back{
    
    
    [ZHttpRequestService POST:@"PayOrder/wxPay" withParameters:@{@"token":[UdStorage getObjectforKey:Userid],
                                                                 @"out_trade_no":orderNum,
                                                        @"total_fee":price,
                                                            @"body":title} success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
                                                                if (succe) {
                                                                    NSDictionary *dic = jsonDic[@"data"][0];
                                                                    [self jumpToBizPayWithDic:dic];
                                                                    back();
                                                                }else{
                                                                    [SXLoadingView showAlertHUD:jsonDic[@"message"] duration:1.5];
                                                                }
                                                            } failure:^(NSError *error) {
                                                                
                                                            } animated:YES withView:nil];
    
}

//银联支付
/**
 *  银联支付
 *
 *  @param Tn 后台返回的tn银联交易流水号
 */
+(void)UnionPayWithTn:(NSString *)Tn withVc:(UIViewController *)target{
    if (Tn!=nil && Tn.length>0) {
        NSLog(@"tn:----  %@", Tn);
        [[UPPaymentControl defaultControl]
         startPay:Tn
         fromScheme:@"UPPayDemo"
         mode:@"00"
         viewController:target];
        
        [target.navigationController popViewControllerAnimated:YES];
        
    }
}


//微信支付
+ (NSString *)jumpToBizPayWithDic:(NSDictionary *)dict {
    
    NSLog(@"weixingxin");
    
    if (![WXApi isWXAppInstalled]) {
        [SXLoadingView showAlertHUD:@"未安装微信" duration:0.5];
        return @"未安装微信";
    }
    
    if(![WXApi isWXAppSupportApi]){
        [SXLoadingView showAlertHUD:@"该版本不支持微信支付" duration:0.5];
        return @"该版本不支持微信支付";
    }
    
    if(dict != nil){
        //调起微信支付
        PayReq* req             = [[PayReq alloc] init];
        req.partnerId           = [dict objectForKey:@"partnerid"];
        req.prepayId            = [dict objectForKey:@"prepayid"];
        req.nonceStr            = [dict objectForKey:@"noncestr"];
        req.timeStamp           = [[dict objectForKey:@"timestamp"] intValue];
        req.package             = [dict objectForKey:@"package"];
        req.sign                = [dict objectForKey:@"sign"];
        [WXApi sendReq:req];
        //日志输出
        NSLog(@"appid=%@\npartid=%@\nprepayid=%@\nnoncestr=%@\ntimestamp=%ld\npackage=%@\nsign=%@",[dict objectForKey:@"appid"],req.partnerId,req.prepayId,req.nonceStr,(long)req.timeStamp,req.package,req.sign );
        return @"";
        
    }else{
        return [dict objectForKey:@"message"];
    }
    
    
}
@end
