//
//  ZCAccountDiscountCell.m
//  shopSN
//
//  Created by chang on 16/7/10.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCAccountDiscountCell.h"

@implementation ZCAccountDiscountCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.backgroundColor = __AccountBGColor;
        [self initSubViews];
    }
    
    return self;
}


- (void)initSubViews {
    
    //***注意宽度设置 -20才能出现右侧边线
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, __kWidth-20, 135)];
    bgView.backgroundColor = HEXCOLOR(0xffffff);
    [self addSubview:bgView];
    bgView.layer.borderWidth = 1.0f;
    bgView.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
    
    //背景图片
    UIImageView *bgIV = [[UIImageView alloc] initWithFrame:bgView.frame];
    [bgView addSubview:bgIV];
    bgIV.image = MImage(@"yhq_bg.png");
    
    
    //1 ￥label
    UILabel *flagLb = [[UILabel alloc] initWithFrame:CGRectMake(20, 70, 15, 25)];
    [bgView addSubview:flagLb];
    flagLb.font = MFont(15);
    flagLb.textColor = __DefaultColor;
    flagLb.text = @"￥";
    
    //2 金额label
    _ca_discountMoneyLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(flagLb)+5, 50, 130, 50)];
    [bgView addSubview:_ca_discountMoneyLb];
//    _ca_discountMoneyLb.backgroundColor = __TestOColor;
    _ca_discountMoneyLb.font = MFont(55);
    _ca_discountMoneyLb.textColor = __DefaultColor;
    
    //3 使用范围label
    _ca_discountReference = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(bgView)-5-115, 40, 115, 20)];
    [bgView addSubview:_ca_discountReference];
//    _ca_discountReference.backgroundColor = __TestGColor;
    _ca_discountReference.font = MFont(13);
    _ca_discountReference.textColor = HEXCOLOR(0x333333);
    _ca_discountReference.textAlignment = NSTextAlignmentCenter;
    
    
    //4 间隔线 label
    UILabel *lineLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(bgView)-5-115, CGRectYH(_ca_discountReference), 115, 10)];
    [bgView addSubview:lineLb];
    lineLb.font = MFont(10);
    lineLb.textColor = HEXCOLOR(0xf0f0f0);
    lineLb.textAlignment = NSTextAlignmentCenter;
    lineLb.text = @"————— · —————";
    
    //5 有效期 label
    _ca_discountTime = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(bgView)-5-115, CGRectYH(lineLb), 115, 15)];
    [bgView addSubview:_ca_discountTime];
    _ca_discountTime.font = MFont(10);
    _ca_discountTime.textColor = HEXCOLOR(0x999999);
    _ca_discountTime.textAlignment = NSTextAlignmentCenter;
    
    
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
