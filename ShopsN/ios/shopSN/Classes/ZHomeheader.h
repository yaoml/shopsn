//
//  ZHomeheader.h
//  shopSN
//
//  Created by imac on 2016/11/18.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZHomeheader : UICollectionReusableView
/** 商品展示 footer 视图 */
@property (nonatomic, strong) UIView *commonGoodsFooterView;


/** 商品展示 footer center图片 */
@property (nonatomic, strong) UIImageView *commonGoodsFooterCenterIV;
@end
