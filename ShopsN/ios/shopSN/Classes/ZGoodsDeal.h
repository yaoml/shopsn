//
//  ZGoodsDeal.h
//  shopSN
//
//  Created by yisu on 16/9/13.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 会员商品 会员旅游 相关页面
 *
 *  商品详情 模型类
 *
 */

#import <Foundation/Foundation.h>

@interface ZGoodsDeal : NSObject

/** 商品详情 商品id */
@property (copy, nonatomic) NSString *goodsID;

/** 商品详情 商品标题 */
@property (copy, nonatomic) NSString *goodsTitle;

/** 商品详情 商品图片地址 */
@property (copy, nonatomic) NSString *goodsPicUrl;

/** 商品详情 商品原价 */
@property (copy, nonatomic) NSString *goodsOriginalPrice;

/** 商品详情 商品现价 */
@property (copy, nonatomic) NSString *goodsNowPrice;

/** 商品详情 商品积分 */
@property (copy, nonatomic) NSString *goodsPoints;


/** 商品详情 商品库存 */
@property (copy, nonatomic) NSString *goodsStocks;

/** 商品详情 是否包邮 */
@property (copy, nonatomic) NSString *goodsShipping;//返回1-包邮 0-不包邮

/** 商品详情 商品套餐 */
@property (copy, nonatomic) NSString *goodsPackage;

/** 商品详情 供应商 */
@property (copy, nonatomic) NSString *goodsShoperID;

/** 商品详情 商品goodsID */
@property (copy, nonatomic) NSString *goodsGoodsID;

//评价信息 com_content  goods_image comment_time  username goods_id pingjia_time
/** 商品详情 评价人账号 */
@property (copy, nonatomic) NSString *goodsJudgeName;

/** 商品详情 评价时间 */
@property (copy, nonatomic) NSString *goodsJudgeTime;

/** 商品详情 评价上传图片 */
@property (copy, nonatomic) NSString *goodsJudgePicUrl;


/** 商品详情 评价内容 */
@property (copy, nonatomic) NSString *goodsJudgeContent;

/** 商品详情 评价商品ID */
@property (copy, nonatomic) NSString *goodsJudgeGoodsID;

//图片数组
/** 商品详情 商品简介图集 */
@property (strong, nonatomic) NSArray *goodsIntrPicArray;

/** 商品详情 商品详情图集 */
@property (strong, nonatomic) NSArray *goodsDetailPicArray;




@end
