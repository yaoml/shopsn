//
//  ZPayConsigneeInfoCell.m
//  shopSN
//
//  Created by chang on 16/7/3.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZPayConsigneeInfoCell.h"

@implementation ZPayConsigneeInfoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //1 titleLb     
        UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(Cell_SPACE, 4, 250, 30)];
        [self addSubview:titleLb];
//        titleLb.backgroundColor = __TestGColor;
        titleLb.font = MFont(15);
        titleLb.textColor = HEXCOLOR(0x333333);
        self.consigneeName = titleLb;
        //2 detailLb
        UILabel *detailLb = [[UILabel alloc] initWithFrame:CGRectMake(Cell_SPACE, CGRectYH(titleLb), __kWidth-50, 30)];
        [self addSubview:detailLb];
//        detailLb.backgroundColor = __TestOColor;
        detailLb.font = MFont(13);
        detailLb.textColor = HEXCOLOR(0x999999);
        
        self.consigneeAddress = detailLb;
    }
    return self;
}







@end
