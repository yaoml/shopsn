//
//  ZChangeAddressViewController.h
//  shopSN
//
//  Created by imac on 16/7/8.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 修改收货地址页面
 *
 *   视图控制器
 *
 */
#import "ZBaseViewController.h"
#import "ReceiveAddressModel.h"

@interface ZChangeAddressViewController : ZBaseViewController

@property (strong,nonatomic) ReceiveAddressModel *addressModel;

@property (strong,nonatomic) NSString *addStr;

@property (strong,nonatomic) UITableView *tableView;

@property (nonatomic) BOOL isDefault;
@end
