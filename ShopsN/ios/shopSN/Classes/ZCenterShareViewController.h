//
//  ZCenterShareViewController.h
//  shopSN
//
//  Created by chang on 16/7/9.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 我的模块
 *
 *  分享shopSN 视图控制器
 *
 */
#import "BaseViewController.h"

@interface ZCenterShareViewController : BaseViewController

/** 会员id */
@property (nonatomic, copy) NSString *memberId;


@end
