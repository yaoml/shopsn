//
//  ZCommonWebViewController.h
//  shopSN
//
//  Created by yisu on 16/10/9.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 账号激活web页面
 *
 *  主视图控制器
 *
 */

#import "BaseViewController.h"



@interface ZCommonWebViewController : BaseViewController

/** 通用web页面 url*/
@property (copy, nonatomic) NSString *urlStr;

/** 通用web页面 title*/
@property (copy, nonatomic) NSString *titleStr;


@end
