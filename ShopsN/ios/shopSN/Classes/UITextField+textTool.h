//
//  UITextField+textTool.h
//  shopSN
//
//  Created by yisu on 16/6/13.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (textTool)

+ (instancetype)setTextFont:(UIFont *)font textColor:(UIColor *)color;

@end
