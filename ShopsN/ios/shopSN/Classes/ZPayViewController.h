//
//  ZPayViewController.h
//  shopSN
//
//  Created by chang on 16/7/2.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 购物车模块
 *
 *  结算页面 视图控制器
 *
 */


#import "ZBaseViewController.h"

@interface ZPayViewController : ZBaseViewController

/** 订单id */
@property (nonatomic, copy) NSString *p_orderId;

/** 支付金额 */
@property (nonatomic, copy) NSString *p_money;


/** 是否 微信支付 */
@property (nonatomic, assign) BOOL isWeixinPay;

/** 用户使用积分 文本 */
@property (nonatomic, copy) NSString *choosePoints;


//**** 按最新接口文件 action[mmediate_payment] 所需参数

/** 支付页面 商品good_id */
@property (nonatomic, copy) NSString *goodsGoodsID;

/** 支付页面 商品套餐 */
@property (nonatomic, copy) NSString *goodsPackage;

/** 支付页面 商品数量 */
@property (nonatomic, copy) NSString *goodsCount;

/** 支付页面 商品good_id */
@property (nonatomic, strong) ZGoodsDeal *deal;//根据需要也可直接传整个商品


- (instancetype)initWithArray:(NSArray *)arr;



@end
