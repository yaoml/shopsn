//
//  ZCommonGoodsIntroductionView.m
//  shopSN
//
//  Created by yisu on 16/8/1.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCommonGoodsIntroductionView.h"


#import "ZCommonGoodsIntroductionCell.h" //普通商品 简介 cell
#import "ZCommonGoodPointsCell.h"        //普通商品 积分 cell
#import "ZCommonGoodsPackageCell.h"      //普通商品 套餐 cell
#import "ZCommonGoodsCountCell.h"        //普通商品 数量 cell

#import "ZCommonGoodsJudgeCell.h"        //普通商品 评价 cell

#import "ZCommonGoodsHeader.h"

@interface ZCommonGoodsIntroductionView ()<UITableViewDataSource, UITableViewDelegate, ZCommonGoodsCountCellDelegate>

{
    UIView *bgView;
    UITableView *_tableView;
    UIView *bottomView;
}



@end


@implementation ZCommonGoodsIntroductionView



- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor redColor];
        
        bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        bgView.backgroundColor = HEXCOLOR(0xffffff);
        [self addSubview:bgView];
        
        
        
        [self initSubViews:bgView];
        
        
    }
    return self;
}





#pragma mark- initSubViews
- (void)initSubViews:(UIView *)view {
    
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(view), CGRectH(view)-40)];
    [view addSubview:_tableView];
    _tableView.backgroundColor = __AccountBGColor;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.dataSource = self;
    _tableView.delegate   = self;
    //刷新控件
    [_tableView addHeaderWithTarget:self action:@selector(tableViewHeaderRefreshAction)];
    [_tableView addFooterWithTarget:self action:@selector(tableViewFootPushAction)];
    //自定义 底部提示文字
    /** 参考
        设置尾部控件的文字
     @property (copy, nonatomic) NSString *footerPullToRefreshText; // 默认:@"上拉可以加载更多数据"
     @property (copy, nonatomic) NSString *footerReleaseToRefreshText; // 默认:@"松开立即加载更多数据"
     @property (copy, nonatomic) NSString *footerRefreshingText; // 默认:@"正在加载更多数据数据..."
     */
    _tableView.footerPullToRefreshText = @"继续拖动 查看图文详情";
    _tableView.footerReleaseToRefreshText = @"松开立即跳转页面";
    _tableView.footerRefreshingText = @"正在跳转页面";
    
    
    
    //底部视图
    bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectH(view)-40, CGRectW(view), 40)];
    [bgView addSubview:bottomView];
    bottomView.backgroundColor = HEXCOLOR(0xdedede);
    //[self addCommonBottomViewSubViews:bottomView];
    
    
}





#pragma mark - ==== 普通商品视图 按钮触发方法 =====
////首页按钮
//- (void)returnHomePageButtonAction {
//    NSLog(@"普通商品简介页面 返回首页");
//    
//}
//
//
////购物车按钮
//- (void)shoppingCarButtonAction {
//    NSLog(@"普通商品简介页面 进入购物车页面");
//}
//
//
//
////会员购物说明按钮
//- (void)noteButtonAction {
//    NSLog(@"普通商品简介页面 进入会员说明页面");
//    
//}



#pragma mark - UITableView DataSource and Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if (section == 0) {
//        return 1;
//    }else{
//        return 5;
//    }
    
    return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        ZCommonGoodsIntroductionCell *cell = [[ZCommonGoodsIntroductionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        
        [cell setGoodsIntrContent:self.goodsDeal];
        
        //分享和收藏
        cell.goodsShareButton.tag = 2411;
        [cell.goodsShareButton addTarget:self action:@selector(buttonShareAndCollectionAction:) forControlEvents:BtnTouchUpInside];
        cell.goodsCollectButton.tag = 2410;
        [cell.goodsCollectButton addTarget:self action:@selector(buttonShareAndCollectionAction:) forControlEvents:BtnTouchUpInside];
        return cell;
    }
    

    
    else if (indexPath.section == 1){//积分
        ZCommonGoodPointsCell *cell = [[ZCommonGoodPointsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        
        [cell setGoodsIntrPoints:self.goodsDeal];
        
        return cell;
    }
    else if (indexPath.section == 2) {//套餐
        ZCommonGoodsPackageCell *cell = [[ZCommonGoodsPackageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        
        [cell setGoodsIntrPackage:self.goodsDeal];
        return cell;
    }
    else { //数量选择
       
        ZCommonGoodsCountCell *cell = [[ZCommonGoodsCountCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        cell.delegate = self;
        
        return cell;
        
    }
}







- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 530;
    }else{

        
        return 54;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {

    
    return 0;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {

    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, 50)];
        headerView.backgroundColor = __TestGColor;
        return headerView;
    }
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, 10)];
    headerView.backgroundColor = [UIColor redColor];
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, 55)];

    
    if (section == 1) {
        footerView.backgroundColor = HEXCOLOR(0xffffff);

        UIButton *chooseJudgeViewBtn = [[UIButton alloc] initWithFrame:CGRectMake((CGRectW(footerView)-70)/2, 15, 70, 25)];
        [footerView addSubview:chooseJudgeViewBtn];
        
        chooseJudgeViewBtn.layer.borderWidth = 1.0f;
        chooseJudgeViewBtn.layer.borderColor = __DefaultColor.CGColor;
        chooseJudgeViewBtn.layer.cornerRadius = 3.0f;
        chooseJudgeViewBtn.titleLabel.font = MFont(10);
        [chooseJudgeViewBtn setTitle:@"查看全部评价" forState:BtnNormal];
        [chooseJudgeViewBtn setTitleColor:__DefaultColor forState:BtnNormal];
        [chooseJudgeViewBtn addTarget:self action:@selector(chooseJudgeViewBtnAction:) forControlEvents:BtnTouchUpInside];
        
    }
    
    
    return footerView;
}


#pragma mark - 分享收藏按钮触发方法
- (void)buttonShareAndCollectionAction:(UIButton *)btn {
    [self.delegate commonGoodsShareAndColloctBtnAciton:btn];
}


#pragma mark - ZCommonGoodsCountCell 代理方法-商品数量况变化情况
- (void)changeGoodsCounts:(NSInteger)counts {
    [self.delegate senderChooseGoodsCounts:counts];
}


#pragma mark - 选择查看全部评价
- (void)chooseJudgeViewBtnAction:(UIButton *)sender {
    NSLog(@"选择查看全部评价");
    [self.delegate showCommonGoodsJudgeView:sender];
}


#pragma mark - 下拉刷新
-(void)tableViewHeaderRefreshAction {
    
    [self refreshCommonGoodsData];
    
}

//刷新页面
- (void)refreshCommonGoodsData {
    NSLog(@"普通商品 简介页面 下拉刷新");
    [_tableView headerEndRefreshing];
}




#pragma mark - 上拉跳转
- (void)tableViewFootPushAction {
    [self pushGoodsDetailView];
}

//代理跳转商品详情页面
- (void)pushGoodsDetailView {
     NSLog(@"普通商品 简介页面 上拉跳转");
    [_tableView footerEndRefreshing];
    [self.delegate showCommonGoodsDetailView];
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
