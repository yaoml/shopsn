//
//  ZHomeheader.m
//  shopSN
//
//  Created by imac on 2016/11/18.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZHomeheader.h"

@interface ZHomeheader ()
{
    BaseView *bgView;

}

@end

@implementation ZHomeheader

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {


        bgView = [[BaseView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        bgView.backgroundColor = HEXCOLOR(0xf0f0f0);
        [self addSubview:bgView];



        [self initSubViews:bgView];

    }
    return self;
}



- (void)initSubViews:(UIView *)view {


    //上边
    UIImageView *lineTopIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(bgView), 10)];
    [view addSubview:lineTopIV];
    lineTopIV.backgroundColor = HEXCOLOR(0xffffff);


    //图片
    _commonGoodsFooterCenterIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, CGRectW(bgView), CGRectH(self)-20)];
    [bgView addSubview:_commonGoodsFooterCenterIV];

    //底线
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectH(bgView)-1, CGRectW(bgView), 1)];
    [view addSubview:lineIV];
    lineIV.backgroundColor = HEXCOLOR(0xdedede);
    
}


@end
