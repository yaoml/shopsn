//
//  ZCenterAccountCell.h
//  shopSN
//
//  Created by chang on 16/7/9.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 账户管理 页面
 *
 *   cell
 *
 */
#import "BaseTableViewCell.h"

@interface ZCenterAccountCell : BaseTableViewCell

/** 图片 */
@property (nonatomic, strong) UIImageView *ca_iconIV;

/** 标题 */
@property (nonatomic, strong) UILabel *ca_titleLb;

/** 详情 */
@property (nonatomic, strong) UILabel *ca_detailLb;



@end
