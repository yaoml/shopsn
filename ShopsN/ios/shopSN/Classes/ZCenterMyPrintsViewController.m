//
//  ZCenterMyPrintsViewController.m
//  shopSN
//
//  Created by yisu on 16/7/11.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCenterMyPrintsViewController.h"

#import "ZCommonGoodsInfoViewController.h"


#import "ZCenterMyPrintsCell.h"     //旅游类 cell
#import "ZCenterMyPrintsGoodsCell.h"//商品类 cell

@interface ZCenterMyPrintsViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    UITableView *_tableView;
    UIView *_clearBGView; //清空视图
    NSArray *_dataSource;
}

@end

@implementation ZCenterMyPrintsViewController
#pragma mark - ==== 页面设置 =====
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"我的足迹";
    _dataSource = @[];
    
    //视图 初始化
    [self initViews];
    
    [self getMyFootprintdata];
}
-(void)getMyFootprintdata{
    NSDictionary *params =@{@"token":[UdStorage getObjectforKey:Userid]};
    [ZHttpRequestService POST:@"MyFootprint/personFootprint" withParameters:params success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            
            _dataSource = jsonDic[@"data"];
            [_tableView reloadData];
            
        }else{
            _dataSource = @[];
            [_tableView reloadData];
        }
    } failure:^(NSError *error) {
        
        [SXLoadingView showAlertHUD:@"失败" duration:SXLoadingTime];
        
    } animated:true withView:nil];
    
}
#pragma mark - 添加导航按钮 【清空】按钮
- (void)addNavigationBar {
    //*** 注意 是super 不是self 否则会崩溃
    [super addNavigationBar];
    
    //自定义按钮类型
    UIButton *rightButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
//    rightButton.backgroundColor = __DefaultColor;
    [rightButton setTitle:@"清除" forState:BtnNormal];
    rightButton.titleLabel.font = MFont(14);
    [rightButton setTitleColor:__DefaultColor forState:0];
    [rightButton addTarget:self action:@selector(rightBtnAction) forControlEvents:BtnTouchUpInside];
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    
    //使用原生系统按钮类型
//    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(rightBtnAction)];
//    [rightItem setTintColor:[UIColor blackColor]];
    
    self.navigationItem.rightBarButtonItem = rightItem;
    
}



//视图 初始化
- (void)initViews {
    
    //顶部边线
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 64, __kWidth, 1)];
    [self.view addSubview:lineIV];
    lineIV.backgroundColor = HEXCOLOR(0xdedede);
//    lineIV.backgroundColor = [UIColor redColor];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 65, __kWidth, __kHeight-65)];
    [self.view addSubview:_tableView];
    _tableView.backgroundColor = __AccountBGColor;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.dataSource = self;
    _tableView.delegate   = self;
    
    
}

#pragma mark- ===== tableView DataSource and Delegate =====

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic = _dataSource[indexPath.row];
    
    NSString *type = dic[@"is_type"];
    
    //is_type 1或空 都作为普通商品
    if ([dic[@"is_type"] isKindOfClass:[NSNull class]]) {
        type = @"1";
    }
    
    if ([type isEqualToString:@"1"]) {
        ZCenterMyPrintsGoodsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyPrintsGoodsCell"];
        if (!cell) {
            cell = [[ZCenterMyPrintsGoodsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyPrintsGoodsCell"];
            
//            cell.cp_shoppingButton
        }
        [cell.cp_iconIV sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",GoodsImageUrl,dic[@"goods_pic"]]]];
        cell.cp_titleLb.text = dic[@"goods_name"];
        cell.cp_priceLb.text = dic[@"goods_price"];
        cell.printFootId = dic[@"id"];
        cell.printGoodsId = dic[@"gid"];
        cell.cp_shoppingButton.hidden = YES;
        
        //删除按钮
        UIButton *delBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectXW(cell.cp_iconIV)+3, CGRectYH(cell.cp_priceLb), 40, 20)];
        [delBtn setTitle:@"删除" forState:0];
        delBtn.titleLabel.font = MFont(13);
        [delBtn setTitleColor:__DefaultColor forState:0];
        delBtn.layer.borderColor = __DefaultColor.CGColor;
        delBtn.layer.borderWidth = 1;
        [cell addSubview:delBtn];
        delBtn.tag = [cell.printFootId integerValue];
        [delBtn addTarget:self action:@selector(respondsToCancelBtn:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    
    ZCenterMyPrintsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyPrintsCell"];
    if (!cell) {
        cell = [[ZCenterMyPrintsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyPrintsCell"];
    }
    [cell.cp_iconIV sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",GoodsImageUrl,dic[@"goods_pic"]]]];
    cell.cp_titleLb.text = dic[@"goods_name"];
    cell.cp_priceLb.text = dic[@"goods_price"];
    cell.printFootId = dic[@"id"];
    cell.printGoodsId = dic[@"gid"];
    cell.cp_shoppingButton.hidden = YES;
    
    
    //删除按钮
    UIButton *delBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectXW(cell.cp_iconIV)+3, CGRectYH(cell.cp_priceLb), 40, 20)];
    [delBtn setTitle:@"删除" forState:0];
    [delBtn setTitleColor:__DefaultColor forState:0];
    delBtn.titleLabel.font = MFont(12);
    delBtn.layer.borderColor = __DefaultColor.CGColor;
    delBtn.layer.borderWidth = 1;
    delBtn.tag = [cell.printFootId integerValue];
    [delBtn addTarget:self action:@selector(respondsToCancelBtn:) forControlEvents:UIControlEventTouchUpInside];
    [cell addSubview:delBtn];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 102;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *dic = _dataSource[indexPath.row];
    
    NSString *type = dic[@"is_type"];
    //is_type 1或空 都作为普通商品
    if ([dic[@"is_type"] isKindOfClass:[NSNull class]]) {
        type = @"1";
    }
    NSString *goodID = dic[@"gid"];
    if ([type isEqualToString:@"1"]) {
        ZCommonGoodsInfoViewController *vc = [[ZCommonGoodsInfoViewController alloc] init];
        vc.comGoodsID = goodID;
        
        [self.navigationController pushViewController:vc animated:YES];
    }
       
    
}


#pragma mark - ==== 按钮触发方法 =====
//清空按钮触发方法
- (void)rightBtnAction {
    NSLog(@"选择 清空记录操作");
    [self clearRecorder];
}
//删除某个足迹
- (void)respondsToCancelBtn:(UIButton *)sender{
    WK(weakSelf)

    [ZHttpRequestService POST:@"MyFootprint/delFootprint" withParameters:@{@"id":@(sender.tag),@"token":[UdStorage getObjectforKey:Userid]} success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
        if (succe) {
            [weakSelf getMyFootprintdata];
        }
    } failure:^(NSError *error) {
        
    } animated:YES withView:nil];
}
/** 初始化 清空浏览记录操作 视图 */
- (void)clearRecorder{
    
    //1 弹出框背景
    _clearBGView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, __kHeight)];
    _clearBGView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.5f];
    [self.view addSubview:_clearBGView];
    
    
    //2 菜单
    UIView *menuView = [[UIView alloc] initWithFrame:CGRectMake((CGRectW(_clearBGView)-260)/2, (CGRectH(_clearBGView)-120)/2, 260, 120)];
    [_clearBGView addSubview:menuView];
    menuView.backgroundColor = HEXCOLOR(0xffffff);
//    menuView.layer.borderWidth = 1.0f;
//    menuView.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
    menuView.layer.cornerRadius = 5.0f;
    
    //2.1 按钮
    NSArray *array = @[@"取消", @"确定"];
    for (int i=0; i<array.count; i++) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(i*(CGRectW(menuView)/2+1), CGRectH(menuView)-41, CGRectW(menuView)/2-1, 41)];
        [menuView addSubview:btn];
        btn.titleLabel.font = MFont(14);
        [btn setTitle:array[i] forState:BtnNormal];
        btn.tag = 120 + i;
//        btn.layer.borderWidth = 1.0f;
//        btn.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
        if (i) {
//            btn.backgroundColor = HEXCOLOR(0xf0f0f0);
            [btn setTitleColor:__DefaultColor forState:BtnNormal];
        }else{
//            btn.backgroundColor = HEXCOLOR(0xf0f0f0);
            [btn setTitleColor:HEXCOLOR(0x999999) forState:BtnNormal];
        }
        
        [btn addTarget:self action:@selector(chooseButtonAciton:) forControlEvents:BtnTouchUpInside];
    }
    
    //2.2 顶部边线
    // *** x坐标 设置42会显得稍粗(按钮上边和线条)，把线条放在按钮视图上即达到设计图效果
    UIImageView *topLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectH(menuView)-41, CGRectW(menuView), 1)];
    [menuView addSubview:topLine];
    topLine.backgroundColor = HEXCOLOR(0xdedede);
    
    //2.3 中间线
    UIImageView *middleLine = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectW(menuView)/2, CGRectH(menuView)-41, 1, 41)];
    [menuView addSubview:middleLine];
    middleLine.backgroundColor = HEXCOLOR(0xdedede);
    
    
    //2.4 提示 确定清空浏览记录？
    UILabel *noteLb = [[UILabel alloc] initWithFrame:CGRectMake(0, (CGRectH(menuView)-42-20)/2, CGRectW(menuView), 20)];
    [menuView addSubview:noteLb];
//    noteLb.backgroundColor = __TestOColor;
    noteLb.font = MFont(15);
    noteLb.textColor = HEXCOLOR(0x333333);
    noteLb.textAlignment = NSTextAlignmentCenter;
    noteLb.text = @"确定清空浏览记录？";
    
    
}

//清空操作按钮触发方法
- (void)chooseButtonAciton:(UIButton *)sender{
    if (sender.tag - 120) {
        //确定操作
        NSLog(@"确定 清空浏览记录");
        WK(weakSelf)
        [ZHttpRequestService POST:@"MyFootprint/emptyFootprint" withParameters:@{@"token":[UdStorage getObjectforKey:Userid]} success:^(id responseObject, BOOL succe, NSDictionary *jsonDic) {
            if (succe) {
                [weakSelf getMyFootprintdata];
            }
        } failure:^(NSError *error) {
            
        } animated:YES withView:nil];
        
    }else {
        //取消操作
        NSLog(@"取消操作");
        
    }
    [_clearBGView removeFromSuperview];
}



#pragma mark - ==== others =====
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
