//
//  ZCMyOrderChangeOrReturnFooter.h
//  shopSN
//
//  Created by yisu on 16/7/14.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 我的模块 我的订单页面
 *
 *  退货/换货 footer
 *
 */
#import "BaseView.h"

@class ZCMyOrderChangeOrReturnFooter;
@protocol ZCMyorderChangeOrReturnFooterDelegate <NSObject>

- (void)orderChangeOrReturnOperatiing:(ZOrderModel *)order andButton:(UIButton *)button;

@end

@interface ZCMyOrderChangeOrReturnFooter : BaseView

/** 退货订单  订单 */
@property (nonatomic, strong) ZOrderModel *order;



/** 退货订单 商品小计价格 */
@property (nonatomic, strong) UILabel *settlePriceLb;

/** 退货订单 商品小计数量 */
@property (nonatomic, strong) UILabel *settleCountLb;

/** 退货订单 商品下单时间 */
@property (nonatomic, strong) UILabel *orderTimeLb;

/** 退货订单footer 代理方法 */
@property (weak, nonatomic) id<ZCMyorderChangeOrReturnFooterDelegate>delegate;

/** 添加旅游类商品信息 */
- (void)addTourismViewInfoDataWithAdultsNum:(NSString *)adultsNum andAdultsPrice:(NSString *)adultsPrice andMinorsNum:(NSString *)minorsNum andMinorsPrice:(NSString *)minorsPrice;

@end
