//
//  ZShoppingCarCell.m
//  shopSN
//
//  Created by yisu on 16/7/1.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZShoppingCarCell.h"

@interface ZShoppingCarCell ()

/** 购物车 商品视图 */
@property (nonatomic, strong) BaseView *shoppingCarGoodsView;



/** 购物车 删除按钮 */
@property (nonatomic, strong) UIButton *shoppingCarDeleteButton;

@end

@implementation ZShoppingCarCell

static NSInteger count = 1;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.backgroundColor = HEXCOLOR(0xf0f0f0);
        
        [self initShoppingCarCellSubViews];
        
    }
    return self;
}

//初始化子视图
- (void)initShoppingCarCellSubViews {
    
    //商品数量
    _sg_shopCount = count;
    
    
    //商品视图
    _shoppingCarGoodsView = [[BaseView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, 150)];
    _shoppingCarGoodsView.backgroundColor = HEXCOLOR(0xffffff);
    [self.contentView addSubview:_shoppingCarGoodsView];
    
    //1 选择按钮
    _shoppingCarCheckButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 10, 35, 65)];
//    _shoppingCarCheckButton.backgroundColor = __TestGColor;
    [_shoppingCarGoodsView addSubview:_shoppingCarCheckButton];
    
    //check_normal   ***注意图片需更新
    [_shoppingCarCheckButton setImage:MImage(@"yuang01") forState:BtnNormal];
    [_shoppingCarCheckButton setImage:MImage(@"yuang02") forState:BtnStateSelected];
    [_shoppingCarCheckButton addTarget:self action:@selector(shoppingCarCheckAction:) forControlEvents:BtnTouchUpInside];
    
    
    
    //2 商品图片
    _sg_imageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectXW(_shoppingCarCheckButton), 10, 65, 65)];
    //_sg_imageView.backgroundColor = __DefaultColor;
    [_shoppingCarGoodsView addSubview:_sg_imageView];
    _sg_imageView.layer.borderWidth = 1.0;
    _sg_imageView.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
    _sg_imageView.contentMode = UIViewContentModeScaleAspectFill;
    _sg_imageView.image = MImage(@"pic12");

    
    //3 商品描述 w 170
    // COSME DECORTE 黛珂 AQ珍萃精颜 悦活洁肤霜 150克
    _sg_descLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_sg_imageView)+5, 10, 160, 35)];
//    _sg_descLb.backgroundColor = __TestGColor;
    [_shoppingCarGoodsView addSubview:_sg_descLb];
    _sg_descLb.numberOfLines = 0;
    _sg_descLb.font = MFont(12);
    _sg_descLb.textColor = HEXCOLOR(0x333333);
    _sg_descLb.text = @"COSME DECORTE 黛珂 AQ珍萃精颜 悦活洁肤霜 150克";
    
    //商品套餐
    _cellIsSet = [[UILabel alloc] initWithFrame:CGRectMake(CGRectX(_sg_descLb), 45, 160, 18)];
    [_shoppingCarGoodsView addSubview:_cellIsSet];
    _cellIsSet.font = MFont(12);
    _cellIsSet.textColor = HEXCOLOR(0x333333);
    _cellIsSet.text = @"套餐：无";
    //商品送的积分
    _cellReturnPoint = [[UILabel alloc] initWithFrame:CGRectMake(CGRectX(_sg_descLb), 63, 160, 18)];
    [_shoppingCarGoodsView addSubview:_cellReturnPoint];
    _cellReturnPoint.font = MFont(12);
    _cellReturnPoint.textColor = HEXCOLOR(0x333333);
    _cellReturnPoint.text = @"送：0积分";
    
    //4 商品现价
    _sg_nowPriceLb = [[UILabel alloc] initWithFrame:CGRectMake(__kWidth-155, 10, 150, 20)];
    //_sg_nowPriceLb.backgroundColor = __TestOColor;
    [_shoppingCarGoodsView addSubview:_sg_nowPriceLb];
    _sg_nowPriceLb.numberOfLines = 0;
    _sg_nowPriceLb.font = MFont(12);
    _sg_nowPriceLb.textColor = HEXCOLOR(0x333333);
    _sg_nowPriceLb.textAlignment = NSTextAlignmentRight;
    _sg_nowPriceLb.text = @"￥589";
    
    //5 商品原价
    _sg_originalPriceLb = [[UILabel alloc] initWithFrame:CGRectMake(__kWidth-55, CGRectYH(_sg_nowPriceLb), 50, 15)];
    //_sg_originalPriceLb.backgroundColor = __TestOColor;
    
//    [_shoppingCarGoodsView addSubview:_sg_originalPriceLb];
    
    _sg_originalPriceLb.numberOfLines = 0;
    _sg_originalPriceLb.font = MFont(12);
    _sg_originalPriceLb.textColor = HEXCOLOR(0x999999);
    _sg_originalPriceLb.textAlignment = NSTextAlignmentRight;
    //_sg_originalPriceLb.text = @"889";
    //添加删除线
    NSString *originalPrice = @"889";
    NSUInteger length = [originalPrice length];
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:originalPrice];
    [attri addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:NSMakeRange(0, length)];
    [attri addAttribute:NSStrikethroughColorAttributeName value:(HEXCOLOR(0x999999)) range:NSMakeRange(0, length)];
    [_sg_originalPriceLb setAttributedText:attri];
    
    
    
    //6 删除按钮 ***看是否使用系统自带的删除
    _shoppingCarDeleteButton = [[UIButton alloc] initWithFrame:CGRectMake(__kWidth-100, CGRectYH(_sg_originalPriceLb)+8, 80, 20)];
    [_shoppingCarGoodsView addSubview:_shoppingCarDeleteButton];
    //_shoppingCarDeleteButton.backgroundColor = __DefaultColor;
    [_shoppingCarDeleteButton setTitle:@"删除" forState:BtnNormal];
    _shoppingCarDeleteButton.titleLabel.font = MFont(14);
    [_shoppingCarDeleteButton setTitleColor:__DefaultColor forState:0];
    _shoppingCarDeleteButton.layer.borderColor = __DefaultColor.CGColor;
    _shoppingCarDeleteButton.layer.borderWidth = 2;
    [_shoppingCarDeleteButton addTarget:self action:@selector(shoppingCarDeleteAction) forControlEvents:BtnTouchUpInside];
    //_shoppingCarDeleteButton.hidden = YES;
    
    
    
    
    //7 商品减少按钮
    UIButton *subBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectXW(_sg_imageView)+5, CGRectYH(_sg_descLb)+35, 25, 20)];
    [_shoppingCarGoodsView addSubview:subBtn];
    //subBtn.backgroundColor = __TestGColor;
    subBtn.layer.borderWidth = 1.0;
    subBtn.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
    //[subBtn setImage:MImage(@"jianshao") forState:BtnNormal];
    //subBtn.titleLabel.font = MFont(20);
    [subBtn setTitle:@"-" forState:BtnNormal];
    [subBtn setTitleColor:HEXCOLOR(0x333333) forState:BtnNormal];
    subBtn.tag = 60;
    [subBtn addTarget:self action:@selector(goodsCountChange:) forControlEvents:BtnTouchUpInside];
    
    //8 商品数量 label
    _sg_countLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(subBtn), CGRectYH(_sg_descLb)+35, 40, 20)];
    [_shoppingCarGoodsView addSubview:_sg_countLb];
    //_sg_countLb.backgroundColor = __TestOColor;
    _sg_countLb.layer.borderWidth = 1.0;
    _sg_countLb.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
    _sg_countLb.font = MFont(14);
    _sg_countLb.textAlignment = NSTextAlignmentCenter;
    _sg_countLb.text = [NSString stringWithFormat:@"%ld",(long)_sg_shopCount];
    
    //9 商品增加按钮
    UIButton *addBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectXW(_sg_countLb), CGRectYH(_sg_descLb)+35, 25, 20)];
    [_shoppingCarGoodsView addSubview:addBtn];
    //addBtn.backgroundColor = __TestGColor;
    addBtn.layer.borderWidth = 1.0;
    addBtn.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
    //[addBtn setImage:MImage(@"jianshao") forState:BtnNormal];
    [addBtn setTitle:@"+" forState:BtnNormal];
    [addBtn setTitleColor:HEXCOLOR(0x333333) forState:BtnNormal];
    addBtn.tag = 61;
    [addBtn addTarget:self action:@selector(goodsCountChange:) forControlEvents:BtnTouchUpInside];
    
    //10 中间边线
    UIImageView *middleLineIV = [[UIImageView alloc] initWithFrame:CGRectMake(10, CGRectYH(addBtn)+10, __kWidth-10, 1)];
    [_shoppingCarGoodsView addSubview:middleLineIV];
    middleLineIV.backgroundColor = HEXCOLOR(0xdedede);
    
    
    
    //11 小计 价格 color (254 65 0) (fe4100)
    _sg_settleNowPriceLb = [[UILabel alloc] initWithFrame:CGRectMake(__kWidth-90, CGRectH(_shoppingCarGoodsView)-35, 85, 25)];
    [_shoppingCarGoodsView addSubview:_sg_settleNowPriceLb];
    //_sg_settleNowPriceLb.backgroundColor = __TestGColor;
    //_sg_settleNowPriceLb.font = MFont(12);
    _sg_settleNowPriceLb.textColor = HEXCOLOR(0xfe4100);
    _sg_settleNowPriceLb.textAlignment = NSTextAlignmentLeft;
    _sg_settleNowPriceLb.text = @"￥589.00";
    
    //12 小计 商品件数
    _sg_settleGoodsCountLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectX(_sg_settleNowPriceLb)-90, CGRectH(_shoppingCarGoodsView)-35, 90, 25)];
    [_shoppingCarGoodsView addSubview:_sg_settleGoodsCountLb];
    //_sg_settleGoodsCountLb.backgroundColor = __TestOColor;
    _sg_settleGoodsCountLb.font = MFont(12);
    _sg_settleGoodsCountLb.textColor = HEXCOLOR(0x999999);
    _sg_settleGoodsCountLb.textAlignment = NSTextAlignmentRight;
    _sg_settleGoodsCountLb.text = [NSString stringWithFormat:@"小计(共%ld件) :",(long)_sg_shopCount];
}


#pragma mark - ==== 按钮触发方法 =====
/**
 *  商品勾选
 */
- (void)shoppingCarCheckAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    
    [self.delegate shoppingCarCell:self didClicSelectButton:sender];
    
    
}

/**
 *  商品删除
 */
- (void)shoppingCarDeleteAction {
    [self.delegate shoppingCarCellDidSelectDeleteGoods:self];
}

/**
 *  商品增加 或 减少
 */
- (void)goodsCountChange:(UIButton *)sender {
    
    NSInteger goodsCount = [_sg_countLb.text integerValue];
    
//    NSLog(@"%@", _sg_countLb.text);
    if (sender.tag - 60) {
        goodsCount += 1;
    }else{
       
        if (goodsCount<2) {
            return;
        }
        goodsCount -= 1;
    }
    NSLog(@"现在count -- %ld", goodsCount);
    
    if (_delegate && [_delegate respondsToSelector:@selector(shoppingCarCell:ChangeGoodsCount:)]) {
        [_delegate shoppingCarCell:self ChangeGoodsCount:goodsCount];
    };
}


@end
