//
//  ZPayTypeCell.h
//  shopSN
//
//  Created by chang on 16/7/4.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 结算页面
 *
 *  支付方式 Cell
 *
 */
#import <UIKit/UIKit.h>

@interface ZPayTypeCell : UITableViewCell

/** 支付方式 选择图标 */
@property (nonatomic, strong) UIButton *pt_iconIV;

/** 支付方式 支付图标 */
@property (nonatomic, strong) UIImageView *pt_payIV;

/** 支付方式 支付标题 */
@property (nonatomic, strong) UILabel *pt_titleLb;

/** 支付方式 支付描述 */
@property (nonatomic, strong) UILabel *pt_detailLb;

@end
