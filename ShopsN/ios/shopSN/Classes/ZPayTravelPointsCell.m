//
//  ZPayTravelPointsCell.m
//  shopSN
//
//  Created by 王子豪 on 16/9/28.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZPayTravelPointsCell.h"

@implementation ZPayTravelPointsCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *title = @[@"使用B积分账户：",@"账户积分余额："];
        
        for (int idx = 0; idx<title.count; idx++) {
            UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake(10, 14+38*idx, 100, 30)];
            
            [self addSubview:titleLb];
            titleLb.font = MFont(12);
            titleLb.textColor = HEXCOLOR(0x333333);
            titleLb.textAlignment = 0;
            titleLb.text = title[idx];
            
            if (idx==0) {
                _pointBTX = [[UITextField alloc] initWithFrame:CGRectMake(CGRectXW(titleLb), CGRectY(titleLb), 100, 30)];
                _pointBTX.layer.borderWidth = 1;
                _pointBTX.layer.borderColor = LH_RGBCOLOR(230, 230, 230).CGColor;
                _pointBTX.font = MFont(13);
                _pointBTX.text = @"0";
                
                //                _pointBTX.keyboardType = UIKeyboardTypeNumberPad;
                [self addSubview:_pointBTX];
            }else if (idx==1){
                _pointALabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(titleLb), CGRectY(titleLb), 100, 30)];
                _pointALabel.textColor = LH_RGBCOLOR(252, 97, 77);
                _pointALabel.font = MFont(12);
                _pointALabel.text = @"A账户：0.00000";
                [self addSubview:_pointALabel];
                _pointBLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_pointALabel)+10, CGRectY(titleLb), 100, 30)];
                _pointBLabel.font = MFont(12);
                _pointBLabel.textColor = _pointALabel.textColor;
                _pointBLabel.text = @"A账户：0.00000";
                [self addSubview:_pointBLabel];
            }
            
        }
    }
    return self;
}

@end
