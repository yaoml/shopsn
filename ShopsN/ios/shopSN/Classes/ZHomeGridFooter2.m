//
//  ZHomeGridFooter2.m
//  shopSN
//
//  Created by yisu on 16/8/11.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZHomeGridFooter2.h"

@interface ZHomeGridFooter2 ()
{
    BaseView *bgView;
    
}

@end

@implementation ZHomeGridFooter2

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        
        bgView = [[BaseView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        bgView.backgroundColor = HEXCOLOR(0xf0f0f0);
        [self addSubview:bgView];
        
        
        
        [self initSubViews:bgView];
        
    }
    return self;
}



- (void)initSubViews:(UIView *)view {
    
    
    //上边
    UIImageView *lineTopIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(bgView), 10)];
    [view addSubview:lineTopIV];
    lineTopIV.backgroundColor = HEXCOLOR(0xffffff);
    
    
    

    
    //图片
    _commonGoodsFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, CGRectW(bgView), CGRectH(bgView)-20)];
    [view addSubview:_commonGoodsFooterView];
    //_commonGoodsFooterView.backgroundColor = __TestOColor;
    
    _commonGoodsFooterCenterIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(_commonGoodsFooterView), 110)];
    [_commonGoodsFooterView addSubview:_commonGoodsFooterCenterIV];

    //_commonGoodsFooterCenterIV.backgroundColor = __TestGColor;
    
    
    //底线
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectH(bgView)-1, CGRectW(bgView), 1)];
    [view addSubview:lineIV];
    lineIV.backgroundColor = HEXCOLOR(0xdedede);
    
}




@end
