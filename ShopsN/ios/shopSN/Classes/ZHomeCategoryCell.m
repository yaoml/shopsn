//
//  ZHomeCategoryCell.m
//  shopSN
//
//  Created by yisu on 16/8/13.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZHomeCategoryCell.h"

@implementation ZHomeCategoryCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 5, frame.size.width, frame.size.height-5)];
        //bgView.backgroundColor = HEXCOLOR(0xffffff);
        //bgView.backgroundColor = __TestGColor;
        [self.contentView addSubview:bgView];
        
        
        //类别图片
        _categoryIV = [[UIImageView alloc] initWithFrame:CGRectMake((CGRectW(bgView)-35)/2, (CGRectH(bgView)-35)/2-5, 35, 35)];
        
        //_categoryIV.backgroundColor = __TestOColor;
        [bgView addSubview:_categoryIV];
        
        //类别文字
        
        _categoryLb = [[UILabel alloc] initWithFrame:CGRectMake((CGRectW(bgView)-60)/2, CGRectYH(_categoryIV), 60, 20)];
        
        _categoryLb.textAlignment = NSTextAlignmentCenter;
        _categoryLb.textColor     = HEXCOLOR(0x333333);
        _categoryLb.font          = MFont(10);
        [bgView addSubview:_categoryLb];
        
        
    }
    return self;
}

//- (void)setCategoryInfo:(ZCategory *)category {
//    _categoryIV.image = MImage(category.imageName);
//    _categoryLb.text = category.title;
//}



@end
