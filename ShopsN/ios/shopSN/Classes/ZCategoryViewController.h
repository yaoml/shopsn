//
//  ZCategoryViewController.h
//  shopSN
//
//  Created by yisu on 16/6/12.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 分类页面
 *
 *  主控制器
 *
 */

#import "ZBaseViewController.h"

@interface ZCategoryViewController : ZBaseViewController

/** 分类页面 判断是否为会员商品 */
@property (nonatomic, assign) BOOL isGoods;


/**分类页面控制器 主类 数组 */
@property (nonatomic, strong) NSArray *categoryArr;


/**分类页面控制器 子类内容 数组 */
@property (nonatomic, strong) NSArray *rowTitleArr;

- (void)getDataWithCategoryArray:(NSArray *)categoryArray andRowTitleArray:(NSArray *)rowTitleArray;

@end
