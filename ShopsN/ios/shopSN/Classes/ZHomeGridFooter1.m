//
//  ZHomeGridFooter1.m
//  shopSN
//
//  Created by yisu on 16/8/11.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZHomeGridFooter1.h"

@implementation ZHomeGridFooter1

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        //直接作为底部线条使用
        BaseView *bgView = [[BaseView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        bgView.backgroundColor = __AccountBGColor;
        [self addSubview:bgView];
        
//        for (int i=0; i<2; i++) {
//            UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, (CGRectH(bgView)-1)*i, CGRectW(bgView), 1)];
//            lineIV.backgroundColor = HEXCOLOR(0xdedede);
//            [bgView addSubview:lineIV];
//        }
        
        //底线
        UIImageView *linIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectH(bgView)-1, CGRectW(bgView), 1)];
        linIV.backgroundColor = HEXCOLOR(0xdedede);
        [bgView addSubview:linIV];
        
    }
    return self;
}


@end
