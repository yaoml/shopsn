//
//  UITextField+textTool.m
//  shopSN
//
//  Created by yisu on 16/6/13.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "UITextField+textTool.h"

@implementation UITextField (textTool)

+ (instancetype)setTextFont:(UIFont *)font textColor:(UIColor *)color {
    UITextField *tf = [[UITextField alloc] init];
    tf.font      = font;
    tf.textColor = color;
    
    return tf;
}

@end
