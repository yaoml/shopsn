//
//  ZCommonGoodsIntroductionCell.h
//  shopSN
//
//  Created by yisu on 16/8/1.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 普通商品 简介页面
 *
 *   普通商品简介内容 cell
 *
 */

#import "BaseTableViewCell.h"

@interface ZCommonGoodsIntroductionCell : BaseTableViewCell

/** 商品图片 */
@property (nonatomic, strong) UIImageView *goodsIV;

/** 商品描述label */
@property (nonatomic, strong) UILabel *goodsDetailLb;

/** 商品现价 */
@property (nonatomic, strong) UILabel *goodsNowPriceLb;


/** 商品原价 */
@property (nonatomic, strong) UILabel *goodsOriginalPriceLb;

/** 商品分享按钮 */
@property (nonatomic, strong) UIButton *goodsShareButton;

/** 商品收藏按钮 */
@property (nonatomic, strong) UIButton *goodsCollectButton;


- (void)setGoodsIntrContent:(ZGoodsDeal *)deal;

@end
