//
//  ZChangeAddressTableViewOtherCell.m
//  亿速
//
//  Created by chang on 16/7/20.
//  Copyright © 2016年 wshan. All rights reserved.
//

#import "ZChangeAddressTableViewOtherCell.h"

@implementation ZChangeAddressTableViewOtherCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style  reuseIdentifier:reuseIdentifier]) {
        [self initView];
    }
    return self;
}


-(void)initView {
    
    UIView *backV = [[UIView alloc]initWithFrame:CGRectMake(0, 0, __kWidth, 80)];
    [self addSubview:backV];
    backV.backgroundColor = [UIColor whiteColor];
    
    _titleLb = [[UILabel alloc]initWithFrame:CGRectMake(15, 10, 90, 30)];
    [backV addSubview:_titleLb];
    _titleLb.textAlignment = NSTextAlignmentLeft;
    _titleLb.font = MFont(15);
    
    _addressTF = [[UITextView alloc]initWithFrame:CGRectMake(95, 10, __kWidth-130, 60)];
    [backV addSubview:_addressTF];

    _addressTF.backgroundColor = [UIColor whiteColor];
//    _addressTF.backgroundColor = __TestGColor;
    _addressTF.font = MFont(15);
    //_addressTF.textAlignment = NSTextAlignmentLeft;
    //_addressTF.hidden = YES;
    
    UIImageView *bottomLineIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, CGRectH(backV)-1, __kWidth, 1)];
    [backV addSubview:bottomLineIV];
    bottomLineIV.backgroundColor = HEXCOLOR(0xdedede);
    
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
