//
//  ZDefaultTableCell.m
//  shopSN
//
//  Created by imac on 16/7/8.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZAddressDefaultTableCell.h"

@implementation ZAddressDefaultTableCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        //self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = __DefaultColor;
        [self initView];
    }
    return self;
}

-(void)initView{
    UIView *backV = [[UIView alloc]initWithFrame:CGRectMake(0, 0, __kWidth, 130)];
    [self addSubview:backV];
    backV.backgroundColor = [UIColor clearColor];

    _nameLb = [[UILabel alloc]initWithFrame:CGRectMake(15, 15, 100, 20)];
    [backV addSubview:_nameLb];
    _nameLb.font =MFont(15);
    _nameLb.textColor = [UIColor whiteColor];
    
    _mobileLb = [[UILabel alloc]initWithFrame:CGRectMake(__kWidth/2, 15, 170, 20)];
    [backV addSubview:_mobileLb];
    _mobileLb.font=MFont(14);
    _mobileLb.textAlignment = NSTextAlignmentLeft;
    _mobileLb.textColor = [UIColor whiteColor];
    
    _addressLb = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectYH(_nameLb), __kWidth-75, 32)];
    [backV addSubview:_addressLb];
    _addressLb.font = MFont(13);
    _addressLb.numberOfLines = 0;
    _addressLb.textColor = [UIColor whiteColor];
    
    UIImageView *lineIV = [[UIImageView alloc]initWithFrame:CGRectMake(15, CGRectYH(_addressLb)+10, __kWidth-15, 1)];
    [backV addSubview:lineIV];
    lineIV.backgroundColor = LH_RGBCOLOR(230, 230, 230);
    
    UIImageView *defIV = [[UIImageView alloc]initWithFrame:CGRectMake(15, CGRectYH(lineIV)+12, 20, 20)];
    [backV addSubview:defIV];
    defIV.layer.cornerRadius = 10;
    defIV.image = [UIImage imageNamed:@"mr_b"];
    
    UILabel *defLb = [[UILabel alloc]initWithFrame:CGRectMake(CGRectXW(defIV)+5, CGRectYH(lineIV)+14, 80, 15)];
    [backV addSubview:defLb];
    defLb.text = @"默认地址";
    defLb.font = MFont(14);
    defLb.textColor = [UIColor whiteColor];
    
    _editBtn = [[UIButton alloc]initWithFrame:CGRectMake(__kWidth-130, CGRectYH(lineIV)+12, 58, 20)];
    [backV addSubview:_editBtn];
    [_editBtn setImage:[UIImage imageNamed:@"bj_b"] forState:BtnNormal];
    _editBtn.imageEdgeInsets = UIEdgeInsetsMake(1, 0, 1, 40);
    [_editBtn setTitle:@"编辑" forState:BtnNormal];
    [_editBtn setTitleColor:[UIColor whiteColor] forState:BtnNormal];
    _editBtn.titleEdgeInsets = UIEdgeInsetsMake(3, -6, 2, 4);
    _editBtn.titleLabel.font = MFont(14);
    [_editBtn addTarget:self action:@selector(editBtnAction:) forControlEvents:BtnTouchUpInside];
    
    _delBtn = [[UIButton alloc]initWithFrame:CGRectMake(CGRectXW(_editBtn)+5, CGRectYH(lineIV)+12, 58, 20)];
    [backV addSubview:_delBtn];
    [_delBtn setImage:[UIImage imageNamed:@"sc_b"] forState:BtnNormal];
    _delBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 40);
    [_delBtn setTitle:@"删除" forState:BtnNormal];
    [_delBtn setTitleColor:[UIColor whiteColor] forState:BtnNormal];
    _delBtn.titleLabel.font = MFont(14);
    _delBtn.titleEdgeInsets = UIEdgeInsetsMake(3, -6, 2, 4);
    [_delBtn addTarget:self action:@selector(delBtnAction:) forControlEvents:BtnTouchUpInside];
    
    UIImageView *bottomV =[[UIImageView alloc]initWithFrame:CGRectMake(0, CGRectYH(_editBtn)+10, __kWidth, 10)];
    [backV addSubview:bottomV];
    bottomV.backgroundColor = LH_RGBCOLOR(230, 230, 230);
    
    UIImageView *bottomLineIV = [[UIImageView alloc]initWithFrame:CGRectMake(0, CGRectH(backV)-1, __kWidth, 1)];
    [backV addSubview:bottomLineIV];
    bottomLineIV.backgroundColor = HEXCOLOR(0xdedede);
    
}

-(void)editBtnAction:(UIButton*)sender{
    [self.delegate edit:sender];
}

-(void)delBtnAction:(UIButton*)sender{
    [self.delegate delAddress:sender];
}

@end
