//
//  ZCategorySubHeader.m
//  亿速
//
//  Created by chang on 16/6/24.
//  Copyright © 2016年 wshan. All rights reserved.
//

#import "ZCategorySubHeader.h"

@implementation ZCategorySubHeader

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        //self.backgroundColor = __TestGColor;
        
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self addSubview:bgView];
        [self initSubViews:bgView];
    }
    
    return self;
}


- (void)initSubViews:(UIView *)bgView {
    _titleLb = [[UILabel alloc] initWithFrame:CGRectMake(Cell_MINISPACE, 0, CGRectW(bgView)-10, CGRectH(bgView))];
    //_titleLb.backgroundColor = __TestOColor;
    _titleLb.font = MFont(13);
    _titleLb.textColor = HEXCOLOR(0x999999);
    [bgView addSubview:_titleLb];
}

@end
