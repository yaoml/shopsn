//
//  ZCMyOrderCell.h
//  shopSN
//
//  Created by yisu on 16/7/14.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface ZCMyOrderCell : BaseTableViewCell

/** 待付款订单  商品图片 */
@property (nonatomic, strong) UIImageView *goodsIV;

/** 待付款订单 商品描述 */
@property (nonatomic, strong) UILabel *descLb;

/** 待付款订单 商品原价 */
@property (nonatomic, strong) UILabel *originalPriceLb;

/** 待付款订单 商品现价 */
@property (nonatomic, strong) UILabel *nowPriceLb;

/** 待付款订单 商品数量 */
@property (nonatomic, strong) UILabel *countLb;


@end
