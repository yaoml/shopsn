//
//  ZPayMoney.h
//  shopSN
//
//  Created by 王子豪 on 16/9/30.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZPayMoney : NSObject

/**
 *  支付宝支付
 *
 *  @param orderNum 订单号
 *  @param title    商品描述
 *  @param price    价格
 *  @param back     回调方法，一般用来跳转页面
 */
+(void)payWithOrderNumber:(NSString *)orderNum title:(NSString *)title price:(NSString *)price complete:(void (^)())back;


/**
 *  微信支付
 *
 *  @param orderNum 订单号
 *  @param title    产品名
 *  @param price    价格
 *  @param back     回调
 */
+(void)weiXinPayWithOrderNumber:(NSString *)orderNum title:(NSString *)title price:(NSString *)price complete:(void (^)())back;


/**
 *  银联支付
 *
 *  @param orderNum 订单号
 *  @param price    价格
 *  @param target   从哪个控制器弹出界面
 *  @param back     
 */
+(void)UnionPayWithOrderNumber:(NSString *)orderNum price:(NSString *)price withVc:(UIViewController *)target complete:(void (^)())back;


@end
