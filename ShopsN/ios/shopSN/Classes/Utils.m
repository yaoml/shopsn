//
//  Utils.m
//  shopSN
//
//  Created by yisu on 16/9/6.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+(NSMutableAttributedString*)changeText:(NSString*)text lineSpace:(CGFloat)lineSpace fontSize:(CGFloat)fontSize;{
    NSMutableAttributedString *attriText=[[NSMutableAttributedString alloc]initWithString:text];
    NSMutableParagraphStyle *paragraphStyle=[[NSMutableParagraphStyle alloc]init];
    [paragraphStyle setLineSpacing:lineSpace];
    [attriText  addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, text.length)];
    [attriText addAttribute:NSFontAttributeName value: MFont(fontSize) range:NSMakeRange(0, text.length)];
    return attriText;
}

+(float)getAttributedStringHeightByText:(NSMutableAttributedString*)text andWidth:(float)width{
    CGSize size=[text boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading context:nil].size;
    return size.height;
    
}

+(CGSize)sizeByText:(NSString*)text andFontSize:(CGFloat)fontSize andWidth:(float)width andIsBold:(BOOL)isBold andIsOriginal:(BOOL)isOriginal{
    UIFont *font;
    if (!isOriginal) {
        fontSize+=0.5;
    }
    if (isBold) {
        font=BFont(fontSize);
    }else{
        font=MFont(fontSize);
    }
    NSDictionary *dic=@{NSFontAttributeName:font};
    CGSize size=[text sizeWithAttributes:dic];
    
    if (size.width>width) {
        size.width=width;
    }
    return size;
}

+(float)heightForString:(NSString *)value FontSize:(CGFloat)fontSize andWidth:(float)width
{
    UIFont *font=MFont(fontSize+0.5);
    CGSize s=[value boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:font} context:nil].size;
    return s.height;
}

+(NSString*)getTextStrByText:(id)text{
    if (!text) {
        return @"";
    }else if (IsNull(text)) {
        return @"";
    }else{
        NSString *nowText=[NSString stringWithFormat:@"%@",text];
        if (IsNilString(nowText)) {
            return @"";
        }else{
            return nowText;
        }
    }
}

+(NSString*)changeToPingyin:(NSString*)originalStr{
    NSMutableString *ms = [[NSMutableString alloc] initWithString:originalStr];
    if ([originalStr length]) {
        if (CFStringTransform((__bridge CFMutableStringRef)ms, 0, kCFStringTransformMandarinLatin, NO)) {
        }
        if (CFStringTransform((__bridge CFMutableStringRef)ms, 0, kCFStringTransformStripDiacritics, NO)) {
        }
    }
    return ms;
}
+(NSDate*)getDateForTimeStamp:(NSString*)timeStamp{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[timeStamp intValue]];
    return date;
}

+(NSString*)getDateStr:(NSDate*)date type:(NSString*)type;{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:type];
    return [formatter stringFromDate:date];
}

//获取当前系统的时间戳
+(NSInteger)getTimeSp{
    NSInteger time;
    NSDate *fromdate=[NSDate date];
    time=(NSInteger)[fromdate timeIntervalSince1970];
    return time;
}
+(NSInteger)getOverTime:(NSString*)timeStamp{
    NSInteger creatTime=[timeStamp integerValue];
    NSInteger nowTime=[Utils getTimeSp];
    NSInteger timeDifference=nowTime-creatTime;
    return timeDifference;
}


+ (UIImage *)buttonImageFromColor:(UIColor *)color{
    
    CGRect rect = CGRectMake(0, 0, 90, 30);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

//+ (BOOL)validateCarNo:(NSString *)carNo
//{
//    NSString *carRegex = @"^[\u4e00-\u9fa5]{1}[a-zA-Z]{1}[a-zA-Z_0-9]{4}[a-zA-Z_0-9_\u4e00-\u9fa5]$";
//    NSPredicate *carTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",carRegex];
//    NSLog(@"carTest is %@",carTest);
//    return [carTest evaluateWithObject:carNo];
//}




@end
