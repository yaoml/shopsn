//
//  ZBaseViewController.m
//  shopSN
//
//  Created by chang on 16/7/2.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZBaseViewController.h"

@interface ZBaseViewController ()
{
    UIButton *rightBtn; //右侧交互按钮
    
}

/** 主页面交互菜单视图 */
@property (nonatomic ,strong) UIView *topMenuView;

@end

@implementation ZBaseViewController

#pragma mark - ==== 页面设置 =====
- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = YES;
    self.tabBarController.tabBar.hidden =YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    self.navigationController.navigationBarHidden = NO;
    self.tabBarController.tabBar.hidden =NO;//该处导致三个页面跳转显示了tabBar
    
    if (_topMenuView.hidden == NO) {
        NSLog(@"需要隐藏 交互视图");
        //1 隐藏交互视图
        _topMenuView.hidden = YES;
        
        //2 恢复中间视图 frame
        CGRect rect_mc   = _mainMiddleView.frame;
        rect_mc.origin.y -= 50;
        [_mainMiddleView setFrame:rect_mc];
        
        //3 修改按钮状态
        rightBtn.selected = NO;
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initNavi];
    
    [self initMainView];
    
    
}

#pragma mark - 自定义导航栏
// 自定义导航栏
- (void)initNavi {
    BaseView *topView = [[BaseView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, 64)];
    topView.backgroundColor = HEXCOLOR(0xffffff);
    [self.view addSubview:topView];
    
    
    UIView *subView = [[UIView alloc] initWithFrame:CGRectMake(0, 25, __kWidth, 40)];
    //subView.backgroundColor = __DefaultColor;
    [topView addSubview:subView];
    
    
    
    //左侧分类 按钮
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(10, 5, 40, 30)];
    [subView addSubview:backView];
    //backView.backgroundColor = __TestOColor;
    UIImageView *backIV = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 20, 20)];
    backIV.image = MImage(@"fanhui.png");
    [backView addSubview:backIV];

    UIButton *backBtn = [[UIButton alloc] initWithFrame:backView.frame];
    [backBtn addTarget:self action:@selector(backAciton) forControlEvents:BtnTouchUpInside];
    [subView addSubview:backBtn];
    
    
    //右侧交互 按钮
    _naviMiddleView = [[UIView alloc] initWithFrame:CGRectMake(35+20, 5, CGRectW(subView)-70-40, 30)];
    [subView addSubview:_naviMiddleView];

    //_naviMiddleView.backgroundColor = __TestOColor;
    [self addNaviSubControllers:_naviMiddleView];
    

    
    
    
    //底部边线
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 63, CGRectW(topView), 1)];
    lineIV.backgroundColor = HEXCOLOR(0xdedede);
    //lineIV.backgroundColor = [UIColor redColor];
    [topView addSubview:lineIV];
    
}


//导航栏 中间视图添加控件
- (void)addNaviSubControllers:(UIView *)middleView {
    //middleView = _naviMiddleView;
    _titleLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectW(middleView), CGRectH(middleView))];
    [middleView addSubview: _titleLb];
    //_titleLb.backgroundColor = __TestOColor;
    _titleLb.textColor = HEXCOLOR(0x333333);
    _titleLb.textAlignment = NSTextAlignmentCenter;
    
    
}


- (void)initMainView {
    //背景
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 64, __kWidth, __kHeight-64)];
    bgView.backgroundColor = HEXCOLOR(0xffffff);
    [self.view addSubview:bgView];
    
    
    //交互 菜单栏
    _topMenuView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(bgView), 50)];
    _topMenuView.backgroundColor = __DefaultColor;
    [bgView addSubview:_topMenuView];
    [self addTopMenuSubView:(_topMenuView)];//添加交互菜单视图
    _topMenuView.hidden = YES;
    
    
    
    //中间页面视图
    _mainMiddleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(bgView), CGRectH(bgView))];
    //_goodsListMainView.backgroundColor = __TestOColor;
    [bgView addSubview:_mainMiddleView];
    
    //[self addGoodsListSubView:_goodsListMainView];//添加其子视图
}




//交互菜单视图
- (void)addTopMenuSubView:(UIView *)menuView {
    
    for (int i=0; i<4; i++) {
        _menuBtnView = [[UIView alloc] initWithFrame:CGRectMake(i*(CGRectW(menuView)/4)+1, 0, CGRectW(menuView)/4-4, CGRectH(menuView))];
        //_menuBtnView.backgroundColor = [UIColor redColor];
        [menuView addSubview:_menuBtnView];
        
        //1 imageView
        UIImageView *btnIV = [[UIImageView alloc] initWithFrame:CGRectMake((CGRectW(_menuBtnView)-20)/2, 10, 20, 20)];
        //btnIV.backgroundColor = __TestOColor;
        [_menuBtnView addSubview:btnIV];
        
        //2 name
        UILabel *btnLb = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectYH(btnIV), CGRectW(_menuBtnView), 20)];
        btnLb.font = MFont(10);
        btnLb.textColor = HEXCOLOR(0xffffff);
        btnLb.textAlignment = NSTextAlignmentCenter;
        [_menuBtnView addSubview:btnLb];
        
        switch (i) {
            case 0:
            {
                btnIV.image = MImage(@"home02.png");
                btnLb.text  = @"首页";
            }
                break;
                
            case 1:
            {
                btnIV.image = MImage(@"fbxs02.png");
                btnLb.text  = @"分类搜索";
                //_menuBtnView.backgroundColor = HEXCOLOR(0x1d9542);
            }
                break;
                
            case 2:
            {
                btnIV.image = MImage(@"buy02.png");
                btnLb.text  = @"购物车";
            }
                break;
                
            case 3:
            {
                btnIV.image = MImage(@"me02.png");
                btnLb.text  = [NSString stringWithFormat:@"我的%@",simpleTitle];
            }
                break;
                
            default:
                break;
        }
        
        //btn
        UIButton *topBtn = [[UIButton alloc] initWithFrame:_menuBtnView.frame];
        [menuView addSubview:topBtn];
        topBtn.tag = 30 + i;
        //topBtn.backgroundColor = HEXCOLOR(0x1d9542);
        
        [menuView addSubview:topBtn];
        
        
    }
    
    
    
}

#pragma mark - ===== 按钮方法 =====

//导航栏左侧 返回按钮 触发方法
- (void)backAciton {
    NSLog(@"返回前页面");
    
    if (self.navigationController.viewControllers.count>1) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        if (self.tabBarController.selectedIndex!=0) {
            [self.tabBarController setSelectedIndex:0];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


//导航栏右侧 交互按钮 触发方法
- (void)rightBtnAciton:(UIButton *)btn {
    NSLog(@"选择交互");
    
    if (btn.selected == YES) {
        _topMenuView.hidden = YES;
        
        //隐藏tabBar
        self.tabBarController.tabBar.hidden =YES;
        
        CGRect rect_mc   = _mainMiddleView.frame;
        rect_mc.origin.y -= 50;
        [_mainMiddleView setFrame:rect_mc];
        
        btn.selected = NO;
        
        
    }else {
        _topMenuView.hidden = NO;
    self.tabBarController.tabBar.hidden =NO;
        
        CGRect rect_mc   = _mainMiddleView.frame;
        rect_mc.origin.y += 50;
        [_mainMiddleView setFrame:rect_mc];
        
        btn.selected = YES;
        
        
        
    }
}

/**
 *  结束编辑
 */
-(void)allResignFirstResponder{
    NSLog(@"allResignFirstResponder====");
    
    [self.view endEditing:YES];
}
-(void)dealloc{
    NSLog(@"界面销毁了");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - others
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
