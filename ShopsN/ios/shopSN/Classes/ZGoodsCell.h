//
//  ZGoodsCell.h
//  shopSN
//
//  Created by yisu on 16/6/27.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 商品列表页面
 *
 *  商品列表cell
 *
 */
#import <UIKit/UIKit.h>

@interface ZGoodsCell : UICollectionViewCell

/** 商品列表 商品图片 */
@property (nonatomic, strong) UIImageView *gl_goodsIV;

/** 商品列表 商品描述 */
@property (nonatomic, strong) UILabel *gl_descLb;

/** 商品列表 商品价格 */
@property (nonatomic, strong) UILabel *gl_pricesLb;

/** 商品列表 商品销量 */
@property (nonatomic, strong) UILabel *gl_salesLb;

@end
