//
//  ZShoppingCarSectionView.m
//  shopSN
//
//  Created by yisu on 16/6/30.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZShoppingCarSectionView.h"

@interface ZShoppingCarSectionView ()

/**
 *  分区全选button
 */
@property (strong,nonatomic) UIButton *sectionCheckButton;
/**
 *  分区标题
 */
@property (strong,nonatomic) UILabel  *sectionLabel;
/**
 *  分区编辑button
 */
@property (strong,nonatomic)  UIButton *sectionEditButton;

@end

@implementation ZShoppingCarSectionView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [self addSubview:bgView];
        bgView.backgroundColor = HEXCOLOR(0xdedede);
        
        //init subviews
        [self initSubViews:bgView];
    }
    return self;
}


- (void)initSubViews:(UIView *)view {
    
    //分区全选按钮
    _sectionCheckButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 35, 35)];
    [view addSubview:_sectionCheckButton];
    [_sectionCheckButton setImage:MImage(@"fuxuan") forState:BtnNormal];
    [_sectionCheckButton setImage:MImage(@"fuxuan2") forState:BtnStateSelected];
    [_sectionCheckButton addTarget:self action:@selector(sectionCheckBtnAction:) forControlEvents:BtnTouchUpInside];
    
    //分区标题
    _sectionLabel = [[UILabel alloc] initWithFrame:CGRectMake(35, 0, CGRectW(view)-35-44, 44)];
    [view addSubview:_sectionLabel];
    //_sectionLabel.backgroundColor = __TestGColor;
    _sectionLabel.font = MFont(15);
    _sectionLabel.textColor = HEXCOLOR(0x333333);
    _sectionLabel.text = @"某某店铺";
    
    
    
    //分区编辑按钮
    _sectionEditButton = [[UIButton alloc] initWithFrame:CGRectMake((CGRectW(view)-44), 0, 44, 44)];
    [view addSubview:_sectionEditButton];
    [_sectionEditButton setTitle:@"编辑" forState:BtnNormal];
    [_sectionEditButton setTitle:@"完成" forState:BtnStateSelected];
    _sectionEditButton.titleLabel.font = MFont(13);
    [_sectionEditButton addTarget:self action:@selector(sectionEditBtnAction:) forControlEvents:BtnTouchUpInside];
    
    
    
    
}




#pragma mark - ==== 按钮触发方法 ======
//分区全选按钮
- (void)sectionCheckBtnAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    [self.delegate shoppingCarSectionView:self didCheckAllSectionGoods:sender.selected];
    
}


//分区标题
- (void)sectionEditBtnAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    [self.delegate shoppingCarSectionView:self didSelectEdit:sender.selected];
}


- (void)setShopTitle:(NSString *)shopTitle {
    _shopTitle = shopTitle;
    _sectionLabel.text = _shopTitle;
}


- (void)setShopCheck:(BOOL)shopCheck {
    _shopCheck = shopCheck;
    _sectionCheckButton.selected = _shopCheck;
}

- (void)setShopEditing:(BOOL)shopEditing {
    _shopEditing = shopEditing;
    _sectionEditButton.selected = _shopEditing;
}

- (void)setShopEditButtonHidden:(BOOL)shopEditButtonHidden {
    _shopEditButtonHidden = shopEditButtonHidden;
    _sectionEditButton.hidden = _shopEditButtonHidden;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
