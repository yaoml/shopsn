//
//  ZCenterShareDomainCell.h
//  shopSN
//
//  Created by yisu on 16/9/18.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 shopSN分享 页面
 *
 *  推广域名 cell
 *
 */
#import "BaseTableViewCell.h"

@interface ZCenterShareDomainCell : BaseTableViewCell

/** 图片 */
@property (nonatomic, strong) UIImageView *cs_iconIV;

/** 标题 */
@property (nonatomic, strong) UILabel *cs_titleLb;


/** 域名内容 */
//@property (nonatomic, strong) UITextView *cs_shareUrlView;
@property (nonatomic, strong) UILabel *cs_shareUrlLabel;

/** 分享按钮 */
@property (nonatomic, strong) UIButton *cs_shareButton;

@end
