//
//  ZTravelDeal.h
//  shopSN
//
//  Created by yisu on 16/9/23.
//  Copyright © 2016年 yisu. All rights reserved.
//
/* 提供 会员旅游 相关页面
 *
 *  商品详情 模型类
 *
 */

#import <Foundation/Foundation.h>

@interface ZTravelDeal : NSObject

/** 旅游商品详情 旅游商品id */
@property (copy, nonatomic) NSString *tourismID;

/** 旅游商品详情 旅游商品标题 */
@property (copy, nonatomic) NSString *tourismTitle;

/** 旅游商品详情 旅游商品图片地址 */
@property (copy, nonatomic) NSString *tourismPicUrl;

/** 旅游商品详情 旅游商品原价 */
@property (copy, nonatomic) NSString *tourismOriginalPrice;

/** 旅游商品详情 旅游商品现价 */
@property (copy, nonatomic) NSString *tourismNowPrice;

/** 旅游商品详情 旅游商品积分 */
@property (copy, nonatomic) NSString *tourismPoints;

/** 旅游商品详情 旅游商品出发地 */
@property (copy, nonatomic) NSString *tourismOriginAddress;

/** 旅游商品详情 供应商id */
@property (copy, nonatomic) NSString *tourismShoperID;





//评价信息
/** 旅游商品详情 评价人账号 */
@property (copy, nonatomic) NSString *tourismJudgeName;

/** 旅游商品详情 评价时间 */
@property (copy, nonatomic) NSString *tourismJudgeTime;

/** 旅游商品详情 评价上传图片 */
@property (copy, nonatomic) NSString *tourismJudgePicUrl;


/** 旅游商品详情 评价内容 */
@property (copy, nonatomic) NSString *tourismJudgeContent;

/** 旅游商品详情 评价商品ID */
@property (copy, nonatomic) NSString *tourismJudgeGoodsID;






/** 旅游商品详情 商品简介图片数组 */
@property (strong, nonatomic) NSArray *tourismIntrPicArray;

/** 旅游商品详情 商品套餐数组 */
@property (strong, nonatomic) NSArray *tourismPakegeArray;


@end
