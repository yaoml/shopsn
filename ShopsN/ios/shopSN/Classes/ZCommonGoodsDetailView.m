//
//  ZCommonGoodsDetailView.m
//  shopSN
//
//  Created by yisu on 16/8/2.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCommonGoodsDetailView.h"

@interface ZCommonGoodsDetailView ()<UIWebViewDelegate>
{
    UIView *bgView;
    UIView *middleView;
    
    
    UIActivityIndicatorView *activityIndicator;
    
    
    
    
   
    
    
    //type1
    //普通商品
    UIView *commonGoodsView;
    
    //商品详情 滚动视图
    UIScrollView *commonScrollView;
    UIView *commonGoodsBottomView;
    
    
    //typ2
    UIWebView *commonGoodsWebView;
    
}

/** 商品详情 url链接 */
@property (copy, nonatomic) NSString *goodsUrlStr;


@end


@implementation ZCommonGoodsDetailView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        //self.backgroundColor = [UIColor greenColor];
        
        bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        bgView.backgroundColor = HEXCOLOR(0xffffff);
        [self addSubview:bgView];
        
        
        [self getData];
        
        
        
    }
    return self;
}


#pragma mark - getData
- (void)getData {
    
    
    
    
    
    
    //[self initSubViewsType1:bgView];//加载方式1
    
    
    
    
}

#pragma mark- ===== initSubViews =====
//- (void)initSubViewsType1:(UIView *)view {
//    
//    
//    //1 scrollView
//    commonGoodsView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(view), CGRectH(view)-40)];
//    [view addSubview:commonGoodsView];
//    //commonGoodsView.backgroundColor = __TestOColor;
//    
//    //[self addCommonGoodsImage:commonScrollView];
//    
//    
//    
//    
//    //2 bottomView
//    commonGoodsBottomView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectH(view)-40, CGRectW(view), 40)];
//    [bgView addSubview:commonGoodsBottomView];
//    commonGoodsBottomView.backgroundColor = HEXCOLOR(0xdedede);
//    //[self addCommonBottomViewSubViews:commonGoodsBottomView];
//    
//}

#pragma mark - ===== 加载方式1相关内容 =====
//#pragma mark - 添加商品详情图片
//- (void)loadCommonGoodsImage:(NSArray *)array {
//    
//    NSLog(@"images:%@",array);
//    CGFloat Width = CGRectW(commonGoodsView);
//    CGFloat Hight = CGRectH(commonGoodsView);
//    // screen 5/5s
//    CGFloat signalContentSizeH = (3*Hight-270);
//    CGFloat scrollContentSizeH = (3*Hight-270)*array.count;
//    CGFloat imageY = Hight-135;
//    NSLog(@"w:%lf===h:%lf==contensize:%lf===imageY:%lf",Width, Hight, scrollContentSizeH, imageY);
//    
//    
//    
//    commonScrollView = [[ UIScrollView alloc] initWithFrame:CGRectMake(0, 0, Width, Hight)];
//    [commonGoodsView addSubview:commonScrollView];
//    commonScrollView.contentSize = CGSizeMake(Width, scrollContentSizeH);
//    //commonScrollView.contentOffset = CGPointMake(0, array.count *(scrollContentSizeH));
//    
//    
//    //goodsIV
//    //urlStr = @"http://www.zzumall.com/ueditor/php/upload/image/20160805/1470369243712348.jpg";
//    //NSLog(@"===>url:%@",urlStr);
//    //    UIImageView *goodsIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, imageY, Width, Hight)];
//    //    goodsIV.contentMode = UIViewContentModeScaleAspectFill;
//    //    [commonScrollView addSubview:goodsIV];
//    //
//    //    NSString *urlStr = [NSString stringWithFormat:@"%@%@",RootURL,array[0]];
//    //
//    //    [goodsIV sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:MImage(@"zp_k.png")];
//    
//    
//    for (int i = 0; i<array.count; i++) {
//        UIImageView *goodsIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, i*(signalContentSizeH)+imageY, Width, Hight)];
//        goodsIV.contentMode = UIViewContentModeScaleAspectFill;
//        NSString *urlStr = [NSString stringWithFormat:@"%@%@",RootURL,array[i]];
//        NSLog(@"url:%@",urlStr);
//        [goodsIV sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:MImage(@"zp_k.png")];
//        [commonScrollView addSubview:goodsIV];
//    }
//    
//    
//    
// 
//    
//}
//



#pragma mark - ===== 加载方式2相关内容 =====

#pragma mark - 加载商品id 获取商品详情url
- (void)loadCommonGoodsID:(NSString *)goodsID {
    
    _goodsUrlStr = [NSString stringWithFormat:@"%@id=%@",GoodsDetailUrl, goodsID];
    NSLog(@"goodsDetailUrl:%@", _goodsUrlStr);
    [self initSubViewsType2:bgView];//加载方式2
}
- (void)initSubViewsType2:(UIView *)view {
    //加载商品详情链接
    NSLog(@"加载普通商品详情页面");
    commonGoodsView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(view), CGRectH(view))];
    [view addSubview:commonGoodsView];
    [self addCommonViewSubViews:commonGoodsView];
}

#pragma mark - 加载普通商品子控件
- (void)addCommonViewSubViews:(UIView *)view {
    
    //加载web视图
    commonGoodsWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, CGRectW(view), CGRectH(view)-40)];
    NSURLRequest *request =[NSURLRequest requestWithURL:[NSURL URLWithString:_goodsUrlStr]];
    //NSURLRequest *request =[NSURLRequest requestWithURL:[NSURL URLWithString:self.travelUrlStr]];
    [view addSubview: commonGoodsWebView];
    [commonGoodsWebView loadRequest:request];
    commonGoodsWebView.delegate = self;
    
    //底部视图
    commonGoodsBottomView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectH(view)-40, CGRectW(view), 40)];
    [bgView addSubview:commonGoodsBottomView];
    commonGoodsBottomView.backgroundColor = HEXCOLOR(0xdedede);
    //[self addCommonBottomViewSubViews:commonGoodsBottomView];
}




#pragma mark - ==== 普通商品视图 按钮触发方法 =====

//- (void)bottmBtnAction:(UIButton *)sender {
//    
//    [self.delegate commonGoodsDetailBottomBtnAction:sender];
//}
//




#pragma mark - ==== webView Delegate =====
- (void) webViewDidStartLoad:(UIWebView *)webView
{
    NSLog(@"webViewDidStartLoad");
    
    //创建UIActivityIndicatorView背底半透明View
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, __kHeight)];
    [view setTag:109];
    [view setBackgroundColor:[UIColor blackColor]];
    [view setAlpha:0.5];
    [self addSubview:view];
    
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)];
    [activityIndicator setCenter:view.center];
    [activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    [view addSubview:activityIndicator];
    
    [activityIndicator startAnimating];
    
}
- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    [activityIndicator stopAnimating];
    UIView *view = (UIView*)[self viewWithTag:109];
    [view removeFromSuperview];
    NSLog(@"webViewDidFinishLoad");
}
- (void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"didFailLoadWithError:%@", error);
    [activityIndicator stopAnimating];
    UIView *view = (UIView*)[self viewWithTag:109];
    [view removeFromSuperview];
    
}



@end
