//
//  ZCMyOrderClosePayFooter.m
//  shopSN
//
//  Created by yisu on 16/7/14.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZCMyOrderClosePayFooter.h"

@implementation ZCMyOrderClosePayFooter
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        _cp_shopCount = 1;
        
        
        //1 背景
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [self addSubview:bgView];
        //        bgView.backgroundColor = __TestOColor;
        
        //2 小计 价格 color (254 65 0) (fe4100)
        _cp_settleNowPriceLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectW(bgView)-125, 10, 120, 15)];
        [bgView addSubview:_cp_settleNowPriceLb];
        _cp_settleNowPriceLb.font = MFont(11);
        _cp_settleNowPriceLb.textColor = __MoneyColor;
        _cp_settleNowPriceLb.textAlignment = NSTextAlignmentRight;
        _cp_settleNowPriceLb.text = @"应付金额:￥176.00";
        [self setSettlePriceLabeltextAttributes:_cp_settleNowPriceLb.text];
        
        //3 小计 商品件数
        _cp_settleGoodsCountLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectX(_cp_settleNowPriceLb)-40, 10, 40, 15)];
        [bgView addSubview:_cp_settleGoodsCountLb];
        //_cp_settleGoodsCountLb.backgroundColor = __TestOColor;
        _cp_settleGoodsCountLb.font = MFont(11);
        _cp_settleGoodsCountLb.textColor = HEXCOLOR(0x333333);
        _cp_settleGoodsCountLb.textAlignment = NSTextAlignmentRight;
        _cp_settleGoodsCountLb.text = [NSString stringWithFormat:@"共%d件",_cp_shopCount];
        
        //4 按钮
        //删除订单按钮 边框color(169 169 169) (0xa9a9a9)
        _cp_deleteOrderBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectW(bgView)-10-75, CGRectYH(_cp_settleNowPriceLb)+10, 75, 25)];
        [bgView addSubview:_cp_deleteOrderBtn];
        _cp_deleteOrderBtn.titleLabel.font = MFont(11);
        //    _cp_deleteOrderBtn.backgroundColor = __TestOColor;
        _cp_deleteOrderBtn.layer.borderWidth = 1.0f;
        _cp_deleteOrderBtn.layer.borderColor = HEXCOLOR(0xa9a9a9).CGColor;
        _cp_deleteOrderBtn.layer.cornerRadius = 3.0f;
        [_cp_deleteOrderBtn setTitle:@"删除订单" forState:BtnNormal];
        [_cp_deleteOrderBtn setTitleColor:HEXCOLOR(0x333333) forState:BtnNormal];
        
        
    }
    return self;
}



- (void)setSettlePriceLabeltextAttributes:(NSString *)text {
    //NSUInteger length = [text length];
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:text];
    //修改前5为颜色
    [attri addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0x333333) range:NSMakeRange(0, 5)];
    
    [_cp_settleNowPriceLb setAttributedText:attri];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
