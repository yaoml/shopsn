//
//  ZShoppingCarSectionView.h
//  shopSN
//
//  Created by yisu on 16/6/30.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "BaseView.h"

@class ZShoppingCarSectionView;
@protocol ShoppingCarSectionViewDelegate <NSObject>

- (void)shoppingCarSectionView:(ZShoppingCarSectionView *)sectionView didCheckAllSectionGoods:(BOOL)_check;
- (void)shoppingCarSectionView:(ZShoppingCarSectionView *)sectionView didSelectEdit:(BOOL)_editing;

@end

@interface ZShoppingCarSectionView : BaseView

/** 店铺标题 */
@property (copy, nonatomic) NSString *shopTitle;

/** 店铺是否勾选 */
@property (nonatomic, assign) BOOL shopCheck;

/** 编辑状态 */
@property (nonatomic, assign) BOOL shopEditing;

/** 编辑按钮是否隐藏 */
@property (nonatomic, assign) BOOL shopEditButtonHidden;


@property(nonatomic, weak) id<ShoppingCarSectionViewDelegate>delegate;


@end
