//
//  ZAccountManagerModel.h
//  shopSN
//
//  Created by 王子豪 on 16/9/12.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZAccountManagerModel : NSObject
/***/
@property (nonatomic,strong) NSDictionary *accountDic;

+(instancetype)shareAccountManagerData;

@end
