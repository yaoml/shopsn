//
//  ZShoppingCarBottomView.h
//  shopSN
//
//  Created by yisu on 16/6/30.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "BaseView.h"

@class ZShoppingCarViewController;
@protocol ZShoppingCarBottomViewDelegate <NSObject>




- (void)didAllCheckButton:(BOOL)_check;

- (void)didSelectPayButton;

@end


@interface ZShoppingCarBottomView : BaseView


/** 结算金额 */
@property (copy, nonatomic) NSString *money;

/** 结算数量 */
@property (copy, nonatomic) NSString *number;

/** 判断是否全选 */
@property (nonatomic, assign) BOOL  allCheck;


/** 合计现价格 */
@property (nonatomic, strong) UILabel *totalNowMoneyLb;

/** 合计原价格 */
@property (nonatomic, strong) UILabel *totalSaveMoneyLb;

/** 结算按钮*/
@property (nonatomic, strong) UIButton *payBtn;


@property (nonatomic, weak) id<ZShoppingCarBottomViewDelegate>delegate;

@end
