//
//  ZChangeAddressTableViewCell.h
//  shopSN
//
//  Created by imac on 16/7/8.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface ZChangeAddressTableViewCell : BaseTableViewCell
/**
 *  类型
 */
@property (strong,nonatomic) UILabel *titleLb;
/**
 *  信息显示
 */
@property (strong,nonatomic) UITextField *detailTV;
/**
 *  选择是否默认地址
 */
@property (strong,nonatomic) UIImageView *chooseIV;

//@property (strong,nonatomic) UITextView *addressTF;

@end
