//
//  ZListGoodsCell.m
//  shopSN
//
//  Created by yisu on 16/9/3.
//  Copyright © 2016年 yisu. All rights reserved.
//

#import "ZListGoodsCell.h"

@implementation ZListGoodsCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self initSubViews];
    }
    return self;
}


- (void)initSubViews {
    
    //_pointStr = @"50积分";
    
    
    //背景
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, __kWidth, 130)];
    [self addSubview:bgView];
    //bgView.backgroundColor = [UIColor orangeColor];
    
    //1 图片
    _iconIV = [[UIImageView alloc] initWithFrame:CGRectMake(10, 15, 100, 100)];
    [bgView addSubview:_iconIV];
    _iconIV.layer.borderWidth = 1.0f;
    _iconIV.layer.borderColor = HEXCOLOR(0xdedede).CGColor;
    _iconIV.image = MImage(@"zp_k.png");
    
    
    //2 描述
    _detailLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_iconIV)+10, 15, CGRectW(bgView)-5-CGRectW(_iconIV)-20, 60)];
    [bgView addSubview:_detailLb];
    //_detailLb.backgroundColor = __TestOColor;
    _detailLb.numberOfLines = 0;
    _detailLb.font = MFont(15);
    _detailLb.textColor = HEXCOLOR(0x333333);
    _detailLb.text = @"日本原装Merries花王 纸尿裤 L54(9-14KG)【4包装】日本原装Merries花王 纸尿裤 L54(9-14KG)【4包装】";
    
    //3 价格
    _priceLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_iconIV)+10, CGRectYH(_detailLb)+10, 100, 20)];
    [bgView addSubview:_priceLb];
    //_priceLb.backgroundColor = __TestOColor;
    _priceLb.font = MFont(15);
    _priceLb.textColor = __MoneyColor;
    _priceLb.text = @"￥472.00元";
    
    //4 积分
    _pointLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectXW(_priceLb), CGRectYH(_detailLb)+10, CGRectW(bgView)-30-CGRectW(_priceLb)-CGRectW(_iconIV), 20)];
    [bgView addSubview:_pointLb];
    //pointLb.backgroundColor = __TestGColor;
    _pointLb.font = MFont(15);
    _pointLb.textColor = HEXCOLOR(0x333333);
    _pointLb.textAlignment = NSTextAlignmentRight;
    
    //_pointLb.text = [NSString stringWithFormat:@"返利：%@",_pointStr];
    //[self setLabeltextAttributes:_pointLb.text];
    
    
    
    //底线
    UIImageView *lineIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectH(bgView)-1, CGRectW(bgView), 1)];
    lineIV.backgroundColor = HEXCOLOR(0xdedede);
    [bgView addSubview:lineIV];
    
    
    
    
}

- (void)setGoodsInfo:(ZGoodsModel *)goods {
    
    //detailLb
    self.detailLb.text = goods.goodsTitle;
    
    //priceLb
    NSString *priceStr = [NSString stringWithFormat:@"￥%@元",goods.goodsNowPrice];
    self.priceLb.text = priceStr;
    
    //pointLb
    NSString *pointStr = [NSString stringWithFormat:@"%@积分",goods.goodsPoints];
    self.pointLb.text = [NSString stringWithFormat:@"送：%@",pointStr];
    [self setPointLabeltextAttributes:self.pointLb];
    
    //iconIV
    NSString *picStr = [NSString stringWithFormat:@"%@%@",GoodsImageUrl, goods.goodsPicUrl];
    [self.iconIV sd_setImageWithURL:[NSURL URLWithString:picStr] placeholderImage:MImage(@"zp_k.png")];
    
    
}


#pragma mark - ===== 设置指定字体大小和颜色 =====
//设置积分内容
- (void)setPointLabeltextAttributes:(UILabel *)label {
    NSString *text = label.text;
    NSUInteger length = [text length];
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:text];
    [attri addAttribute:NSForegroundColorAttributeName value:__MoneyColor range:NSMakeRange(2, length-2)];
    [label setAttributedText:attri];
    
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
